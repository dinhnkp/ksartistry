import clsx from "clsx";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import PopUpForm  from "../Contact/PopupForm";
import { hostBE } from "../../types/host";
import { showModal } from "../../reducers/modal";
import { useAppDispatch } from "../../hooks/hook";
import SilderNews from "../Home/SliderNews";

export default function ContactBottom() {

    const navigate = useNavigate();
    const [t] = useTranslation();
    const { ref, isInView } = useInView();
    const dispatch = useAppDispatch();

    const showModalContact = () => {
        dispatch(showModal(<PopUpForm />))
      };

    return (
        <div className="lssm:h-auto sc<992:h-[579px] max-w-full lg:my-24 lssm:my-14 md:my-16 lssm:px-[24px] md:px-[80px] xl:px-[50px] w-1920:px-[162px]" ref={ref}>
            <div className="flex flex-col justify-center items-center">
                <span className="Valky text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] uppercase font-bold">{t("home.topic.Body")}</span>
                <div
                className={clsx(
                    "text-center lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-[32px] sc991:mb-3 text-[#64483B]",
                    { "animate__animated animate__flash": isInView }
                )}
                dangerouslySetInnerHTML={{ __html: t("home.body_sub") }}
                ></div>
            </div>
            <SilderNews/>
            <div className="bg-[#F5ECE5] max-w-full lg:my-24 lssm:my-14 md:my-16 xl:py-12 md:py-10 lsm-320:py-8 rounded-[24px]">
                <h1 className="text-center Valky text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] uppercase font-bold">{t("home.topic.gifts")}</h1>
                <p className="h-auto font-bold 2xl:mb-5 text-base sm-480:text-xl mb-3 text-[#2F2D38] line-clamp-1 mt-5 text-center">{t("home.topic.store")}</p>
            </div>
        </div>
    )

}