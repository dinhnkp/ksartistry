import clsx from "clsx";
import React, { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { Autoplay, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/grid";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import Button from "../../components/Button/index";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import useInView from "../../hooks/useInView";
import { getProductPublic } from "../../reducers/productPublic";

function chunkArray(myArray: any[], chunk_size: number) {
  let index = 0;
  let arrayLength = myArray.length;
  let tempArray = [];

  for (index = 0; index < arrayLength; index += chunk_size) {
    let myChunk = myArray.slice(index, index + chunk_size);
    // Do something if you want with the group
    tempArray.push(myChunk);
  }

  return tempArray;
}

export default function SliderNews() {
  const { isInView } = useInView();
  const dispatch = useAppDispatch();
  const productList = useAppSelector((state) => state.productPublic);
  const navigate = useNavigate();
  const [t] = useTranslation();
  const { categoryFilter } = useAppSelector((state) => state.categorySlice);
  const numberCategory = categoryFilter.length > 0 ? categoryFilter[0] : 0;
  const width = useMemo(() => {
    return window.innerWidth;
  }, []);

  useEffect(() => {
    dispatch(
      getProductPublic({
        type: "en",
        search: "",
        categoryId: numberCategory,
        page: 0,
        limit: 9,
        sort: 0,
      })
    );
  }, []);

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  return (
    <div className="flex-1 flex items-center relative mt-10">
      <Swiper
        slidesPerView={
          width >= 1850
            ? 2
            : width >= 992
            ? 2
            : width >= 768
            ? 1
            : width >= 380
            ? 1
            : width >= 330
            ? 1
            : 1
        }
        spaceBetween={30}
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        onSwiper={(swiper: any) => {
          setTimeout(() => {
            swiper.params.navigation.prevEl = navigationPrevRef.current;
            swiper.params.navigation.nextEl = navigationNextRef.current;

            swiper.navigation.destroy();
            swiper.navigation.init();
            swiper.navigation.update();
          });
        }}
         autoplay={{
          delay: 1800
         }}
        modules={[Navigation, Autoplay]}
        className="slider-company-home max-md:hidden"
      >
        {chunkArray(productList.productPublicList, 2).map((_, index) => {
          if ((index + 1) % 2 === 0 && width < 768) return "";
          return width < 768 ? (
            <SwiperSlide className="slider-company-home-item " key={index}>
              {_.map((item: any, idx: number) => {
                return (
                  <div key={idx}>
                    <div className="sc>768:max-h-[74px]">
                      <img
                        src={item?.avatarUrl ?? ""}
                        className="w-full"
                        alt={item?.avatarUrl ?? ""}
                      />
                    </div>
                    {index + 1 < productList.productPublicList.length && (
                      <div className="mt-[24px] sc>768:max-h-[74px]">
                        <img
                          src={
                            productList.productPublicList[index + 1]
                              ?.avatarUrl ?? ""
                          }
                          className="w-full"
                          alt={
                            productList.productPublicList[index + 1]
                              ?.avatarUrl ?? ""
                          }
                        />
                      </div>
                    )}
                  </div>
                );
              })}
            </SwiperSlide>
          ) : (
            <SwiperSlide className="slider-company-home-item" key={index}>
              <div className="flex flex-col gap-y-5 bg-[#FCF5EF]">
                {_.map((item: any, idx: number) => {
                  return (
                    <div className="bg-[#F5ECE5] lg:p-6 m992:p-[5%] md:p-6" key={idx}>
                      <img
                        className="w-1920:w-[300px] 2xl:w-[140px] xl:w-[120px] lg:w-[120px] m992:w-[110px] sc991:w-[60px] md:w-[200px] w-1920:h-[300px] 2xl:h-[140px] xl:h-[120px] lg:h-[120px] m992:h-[110px] sc991:h-[60px] md:h-[200px] rounded-[10%]"
                        src={item?.avatarUrl ?? ""}
                        alt={item?.avatarUrl ?? ""}
                      />
                      <div className="flex flex-col !justify-between !items-start ml-5">
                        <p
                          className={clsx(
                            "h-auto font-bold 2xl:mb-5 text-base sm-480:text-xl mb-3 text-bg_blue_bold line-clamp-1",
                            {
                              "animate__animated animate__fadeInDown": isInView,
                            }
                          )}
                        >
                          {item?.titleEn ?? ""}
                        </p>
                        <p
                          className={clsx(
                            "sm-480:text-base text-sm h-auto font-normal text-text-gray line-clamp-2"
                          )}
                        >
                          {item?.descriptionEn ?? ""}
                        </p>
                        <Button
                          onClick={() =>
                            navigate(`/chitietsanpham/${item?.id}`)
                          }
                          color="pink"
                          className="2xl:mt-3 mt-5 bg-white sc991:mb-6 lg:h-[40px] rounded-[12px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[100px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                        >
                          <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 lg:text-px16">
                            {t("button.see_more")}
                          </span>
                        </Button>
                      </div>
                    </div>
                  );
                })}
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
      <Swiper
        slidesPerView={
          width >= 1850
            ? 2
            : width >= 992
            ? 2
            : width >= 767
            ? 1
            : 1
        }
        spaceBetween={30}
        // slidesPerGroup={1}
        loop={false}
        loopFillGroupWithBlank={true}
        autoplay={{
          delay: 1800
         }}
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        onSwiper={(swiper: any) => {
          setTimeout(() => {
            swiper.params.navigation.prevEl = navigationPrevRef.current;
            swiper.params.navigation.nextEl = navigationNextRef.current;

            swiper.navigation.destroy();
            swiper.navigation.init();
            swiper.navigation.update();
          });
        }}
        modules={[Navigation, Autoplay]}
        className="slider-company-home md:hidden"
      >
        {productList.productPublicList.map((item, index) => {
          if ((index + 1) % 2 === 0 && width > 768) return "";
          return width > 768 ? (
            <SwiperSlide className="slider-company-home-items " key={item.id}>
              <div className="sc>768:max-h-[74px]">
                <img
                  src={item?.avatarUrl ?? ""}
                  className="w-full"
                  alt={item?.avatarUrl ?? ""}
                />
              </div>
              {
                index + 1 < productList.productPublicList.length && (
              <div className="mt-[24px] sc>768:max-h-[74px]">
                <img
                  src={productList.productPublicList[index + 1]?.avatarUrl ?? ""}
                  className="w-full"
                  alt={productList.productPublicList[index + 1]?.avatarUrl ?? ""}
                />
              </div>

                )
              }
            </SwiperSlide>
          ) : (
            <SwiperSlide className="slider-company-home-items" key={item.id}>
              <div className="flex flex-col gap-y-5 bg-[#FCF5EF]">
                    <div className="bg-[#F5ECE5] lg:p-6 m992:p-[5%] md:p-6">
                      <img
                        className="w-full h-[347px] object-cover rounded-[30px] mb-4"
                        src={item?.avatarUrl ?? ""}
                        alt={item?.avatarUrl ?? ""}
                      />
                      <div className="flex flex-col !justify-between !items-start p-[5%]">
                        <p
                          className={clsx(
                            "h-auto font-bold 2xl:mb-5 text-base sm-480:text-xl mb-3 text-bg_blue_bold line-clamp-1",
                            {
                              "animate__animated animate__fadeInDown": isInView,
                            }
                          )}
                        >
                          {item?.titleEn ?? ""}
                        </p>
                        <p
                          className={clsx(
                            "sm-480:text-base text-sm h-auto font-normal text-text-gray line-clamp-2"
                          )}
                        >
                          {item?.descriptionEn ?? ""}
                        </p>
                        <Button
                          onClick={() =>
                            navigate(`/chitietsanpham/${item?.id}`)
                          }
                          color="pink"
                          className="2xl:mt-3 mt-2 max-md:mt-5 bg-white sc991:mb-6 lg:h-[40px] m992:h-[30px] max-md:h-[35px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[100px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                        >
                          <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 lg:text-px16 m992:text-px14 lsm-320:text-[14px]">
                            {t("button.see_more")}
                          </span>
                        </Button>
                      </div>
                    </div>
              </div>  
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
}
