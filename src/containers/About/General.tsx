import clsx from "clsx";
import { useTranslation } from "react-i18next";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { hostBE } from "../../types/host";
import { showModal } from "../../reducers/modal";
import BourchureIndex from "../../components/Brouchure/BourchureIndex";

const data1 = [
  {
    title : "NAILS SERVICE",
    service : [
      {
        titleservice : "NAILS SERVICE",
        service : [
          {
            package : "Acrylic + gel colour",
            des : "",
            price : "60",
            pricesession : ""
          },
          {
            package : "Gel extensions tips + gel colour",
            des : "",
            price : "60",
            pricesession : ""
          },
          {
            package : "Sns on nature nails",
            des : "",
            price : "50",
            pricesession : ""
          },
          {
            package : "Sns on extensions nails",
            des : "",
            price : "60",
            pricesession : ""
          },
          {
            package : "Manicure gel nails",
            des : "",
            price : "40",
            pricesession : ""
          },
          {
            package : "Express gel nails",
            des : "",
            price : "25",
            pricesession : ""
          },
          {
            package : "Design from",
            des : "",
            price : "5",
            pricesession : ""
          },
          {
            package : "French tips extra",
            des : "",
            price : "10",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "WAXING SERVICE",
        service : [
          {
            package : "Eyebrows wax",
            des : "",
            price : "10",
            pricesession : ""
          },
          {
            package : "Upper lips wax",
            des : "",
            price : "10",
            pricesession : ""
          },
          {
            package : "Chin",
            des : "",
            price : "10",
            pricesession : ""
          },
          {
            package : "Full face",
            des : "",
            price : "30",
            pricesession : ""
          },
          {
            package : "Under arm",
            des : "",
            price : "15",
            pricesession : ""
          },
          {
            package : "Full leg",
            des : "",
            price : "55",
            pricesession : ""
          },
          {
            package : "Haft leg",
            des : "",
            price : "30",
            pricesession : ""
          },
          {
            package : "Full arm",
            des : "",
            price : "40",
            pricesession : ""
          },
          {
            package : "Haft arm",
            des : "",
            price : "25",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "TINTING SERVICE",
        service : [
          {
            package : "Lashes tint",
            des : "",
            price : "15",
            pricesession : ""
          },
          {
            package : "Eyebrows tint",
            des : "",
            price : "15",
            pricesession : ""
          },
          {
            package : "Eyebrows and lashes tint",
            des : "",
            price : "25",
            pricesession : ""
          },
          {
            package : "Eyebrows and lashes tint",
            des : "",
            price : "20",
            pricesession : ""
          }
        ]
      }
    ]
  },
  {
    title : "LASHES EXTENTIONS",
    service : [
      {
        titleservice : "NATURAL STYLE",
        service : [
          {
            package : "Nature classic lashes",
            des : "",
            price : "65",
            pricesession : ""
          },
          {
            package : "Full classic lashes",
            des : "",
            price : "70",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "PRE FAN MAKES LASHES",
        service : [
          {
            package : "Nature volume",
            des : "",
            price : "75",
            pricesession : ""
          },
          {
            package : "Hyubird style",
            des : "",
            price : "80",
            pricesession : ""
          },
          {
            package : "Mega volume",
            des : "",
            price : "85",
            pricesession : ""
          },
          {
            package : "Wispy style lashes",
            des : "",
            price : "90",
            pricesession : ""
          },
          {
            package : "Wet wispy style looks",
            des : "",
            price : "95",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "HAND MADE STYLE",
        service : [
          {
            package : "Nature volume",
            des : "",
            price : "80",
            pricesession : ""
          },
          {
            package : "Hyubird style",
            des : "",
            price : "90",
            pricesession : ""
          },
          {
            package : "Mega volume",
            des : "",
            price : "80",
            pricesession : ""
          },
          {
            package : "Other free style make anime lashes",
            des : "",
            price : "75",
            pricesession : ""
          }
        ]
      }
    ]
  },
  {
    title : "BROWS & LIPS TATTOOS",
    service : [
      {
        titleservice : "BROWS SERVICE",
        service : [
          {
            package : "Shading powder",
            des : "",
            price : "325 - 1st session",
            pricesession : "150 - 2nd session"
          },
          {
            package : "Hair straker",
            des : "",
            price : "650 - 1st session",
            pricesession : "150 - 2nd session"
          },
          {
            package : "Ombre powder brows",
            des : "",
            price : "399 - 1st session",
            pricesession : "150 - 2nd session"
          }
        ]
      },
      {
        titleservice : "LIPS TINTED",
        service : [
          {
            package : "Dark lips neutralisation",
            des : "",
            price : "399 - 1st session",
            pricesession : "175 - 2nd session"
          },
          {
            package : "Lips blushing",
            des : "",
            price : "350 - 1st session",
            pricesession : "150 - 2nd session"
          },
          {
            package : "Lips cover",
            des : "",
            price : "350 - 1st session",
            pricesession : "100 - 2nd session"
          },
          {
            package : "Lip colour removal",
            des : "Combo 1-3 times",
            price : "400",
            pricesession : ""
          },
          {
            package : "Eyeliner",
            des : "",
            price : "400",
            pricesession : ""
          }
        ]
      }
    ]
  },
  {
    title : "HAIR WASH AND MASSAGE",
    service : [
      {
        titleservice : "HAIR SERVICE",
        service : [
          {
            package : "Hair wash and head massage",
            des : "",
            price : "50",
            pricesession : ""
          },
          {
            package : "Hair wash and head + shoulder massage",
            des : "",
            price : "80",
            pricesession : ""
          },
          {
            package : "Hair & Face wash and clean",
            des : "",
            price : "50",
            pricesession : ""
          }
        ]
      }
    ]
  },
  {
    title : "OTHER SERVICE",
    service : [
      {
        titleservice : "LASHES LIFT",
        service : [
          {
            package : "Lashes lift and tint",
            des : "",
            price : "70",
            pricesession : ""
          },
          {
            package : "Lashes lift only",
            des : "",
            price : "60",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "LASHES REMOVAL",
        service : [
          {
            package : "Lashes removal",
            des : "",
            price : "25",
            pricesession : ""
          },
          {
            package : "Lashes removal and add on",
            des : "",
            price : "10",
            pricesession : ""
          },
          {
            package : "Second time",
            des : "",
            price : "Free",
            pricesession : ""
          }
        ]
      }
    ]
  },
  {
    title : "SEMI PERMANENT MAKE UP TATTOO TRANING 1:1",
    service : [
      {
        titleservice : "POWDER BROWS",
        service : [
          {
            package : "Basic course",
            des : "5 days",
            price : "2500",
            pricesession : ""
          },
          {
            package : "Advance course",
            des : "3 days",
            price : "3500",
            pricesession : ""
          },
          {
            package : "Basic to Advance course",
            des : "7 days",
            price : "5000",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "HAIRSTROKER COURSE",
        service : [
          {
            package : "Basic to Advance course",
            des : "7 days",
            price : "5500",
            pricesession : ""
          }
        ]
      },
      {
        titleservice : "SLIPS COURSE",
        service : [
          {
            package : "Basic course",
            des : "5 days",
            price : "2500",
            pricesession : ""
          },
          {
            package : "Advance course",
            des : "5 days",
            price : "5500",
            pricesession : ""
          },
          {
            package : "Basic to Advance course",
            des : "10 days",
            price : "7000",
            pricesession : ""
          },
          {
            package : "Colour course",
            des : "( how to use colour in PMU ) cover old eyebrows and lips, Use colour to treat dark lips, How to use green , blue , purple for lips",
            price : "1500",
            pricesession : ""
          }
        ]
      }
    ]
  }
]

export default function General() {
  const [t] = useTranslation();
  const topInView = useInView();
  const bottomInView = useInView();
  const dispatch = useAppDispatch();
  const translate = useAppSelector((state) => state.translateSlice);

  const showBouchure = () => {
    dispatch(showModal(<BourchureIndex />));
  };

  return (
    <div className="relative h-auto">
      <div className="mt-[60px] sc>768:mt-[35px]">
        <div className="flex sc991:flex-col text-justify mb-20 sc>768:mb-14">
          {/* <div className="flex-1 aspect-[587/442] object-cover animate__animated animate__fadeInLeft">
            <img
              src={`${hostBE}/fe/cathyle.png`}
              className="sc991:w-full rounded-xl"
              alt=""
            />
          </div> */}
          <div className="flex-1 animate__animated animate__fadeInRight">
            <h3 className="text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] lssm:mb-[20px] sc>768:text-center md:mb-[40px] sc>768:mt-[24px] sc991:mt-[35px] uppercase font-medium">
              {t("about.general.about_us")}
            </h3>
            <div
              className="lssm:text-px14 md:text-px16 text-justify"
              dangerouslySetInnerHTML={{
                __html: t("about.general.we_description"),
              }}
            ></div>
          </div>
        </div>

        <div className="mb-20 sc>768:mb-14">
          <div className="flex-1  animate__animated animate__fadeInLeft">
            <h3 className="text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] lssm:mb-[20px] sc>768:text-center md:mb-[40px] uppercase font-medium">
              {t("about.general.honored")}
            </h3>
            <div
              className="lssm:text-px14 md:text-px16 text-justify sc991:mb-8"
              dangerouslySetInnerHTML={{
                __html: t("about.general.ks_description"),
              }}
            >
            </div>
          </div>

          <div className="grid grid-cols-12 2xl:my-16 xl:my-12 lg:my-8 max-lgs:my-8 gap-5">
            <div className="lg:col-span-5 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/giaovien4.jpg" alt="" />
            </div>
            <div className="lg:col-span-7 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <div className="grid grid-cols-12 gap-5">
                <div className="lg:col-span-6 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/giaovien1.jpg" alt="" />
                </div>
                <div className="lg:col-span-6 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/giaovien5.jpg" alt="" />
                </div>
                <div className="lg:col-span-6 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/giaovien3.jpg" alt="" />
                </div>
                <div className="lg:col-span-6 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/giaovien2.jpg" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="mb-20 sc>768:mb-14">
          <div className="flex-1  animate__animated animate__fadeInLeft">
            <h3 className="text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] lssm:mb-[20px] sc>768:text-center md:mb-[40px] uppercase font-medium">
              {t("about.general.major_tech")}
            </h3>
            <div
              className="lssm:text-px14 md:text-px16 text-justify sc991:mb-8"
              dangerouslySetInnerHTML={{
                __html: t("about.general.major_description"),
              }}
            >
            </div>
            <p className="lssm:text-px16 md:text-px18 text-justify font-semibold mb-5">{t("about.general.achivements")}</p>
            <p className="lssm:text-px14 md:text-px16 text-justify mb-4">{t("about.general.certificate1")}</p>
            <p className="lssm:text-px14 md:text-px16 text-justify mb-4">{t("about.general.certificate2")}</p>
            <p className="lssm:text-px14 md:text-px16 text-justify mb-4">{t("about.general.certificate3")}</p>
          </div>

          <div className="grid grid-cols-12 2xl:my-16 xl:my-12 lg:my-8 max-lgs:my-8 gap-5">
                <div className="lg:col-span-4 sm:col-span-6 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/Certificate-2.jpg" alt="" />
                </div>
                <div className="lg:col-span-4 sm:col-span-6 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/Certificate-1.jpg" alt="" />
                </div>
                <div className="lg:col-span-4 sm:col-span-12 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/Certificate-3.jpg" alt="" />
                </div>
          </div>
        </div>

        <div ref={bottomInView.ref} className="mb-20 sc>768:mb-14">
          <div className="flex-1 sc<992:mr-[50px]  animate__animated animate__fadeInRight">
            <h3 className="text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] lssm:mb-[20px] sc>768:text-center md:mb-[40px] sc>768:mt-[24px] sc991:mt-[35px] uppercase font-medium">
              {t("about.general.employe_count")}
            </h3>
            <div
              className="lssm:text-px14 md:text-px16 text-justify"
              dangerouslySetInnerHTML={{
                __html: t("about.general.course"),
              }}
            ></div>
          </div>
          <div className="grid grid-cols-12 2xl:my-16 xl:my-12 lg:my-8 max-lgs:my-8 gap-5">
            <div className="lg:col-span-6 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/train-1.jpg" alt="" />
            </div>
            <div className="lg:col-span-6 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/train-2.jpg" alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
