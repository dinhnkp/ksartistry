import clsx from "clsx"
import useInView from "../../hooks/useInView"
import { hostBE } from "../../types/host"

export default function Banner () {
const {isInView, ref} = useInView()
    return <div ref={ref} className="!relative w-full overflow-hidden aspect-[1920/600] h-[600px] flex justify-center sc>768:h-[200px] items-center"
    style={{
        backgroundImage: `url('${hostBE}/fe/introBanner-1.png')`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}>
        <div className="absolute h-full top-0 right-0 w-full">
            <div className="w-full h-full opacity-30 bg-[#000000] aspect-[1920/600]"></div>
        </div>
        <h3 className="text-white Valky h-full flex justify-center items-end iframeVideo z-50 2xl:text-[56px] xl:text-[46px] lg:text-[35px] md:text-[30px] sm:text-[24px] pb-[5%] animate__animated animate__fadeInDown">K’S BEAUTY ARTISTRY </h3>
    </div>
}