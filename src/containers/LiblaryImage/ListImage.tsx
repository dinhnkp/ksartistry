import { useEffect } from "react";
import clsx from "clsx";
import useInView from "../../hooks/useInView";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { getAlbum } from "../../reducers/album";
import { hostBE } from "../../types/host";

const data1 = [
    {
      title : "NAILS SERVICE",
      service : [
        {
          titleservice : "NAILS SERVICE",
          service : [
            {
              package : "Acrylic + gel colour",
              des : "",
              price : "60",
              pricesession : ""
            },
            {
              package : "Gel extensions tips + gel colour",
              des : "",
              price : "60",
              pricesession : ""
            },
            {
              package : "Sns on nature nails",
              des : "",
              price : "50",
              pricesession : ""
            },
            {
              package : "Sns on extensions nails",
              des : "",
              price : "60",
              pricesession : ""
            },
            {
              package : "Manicure gel nails",
              des : "",
              price : "40",
              pricesession : ""
            },
            {
              package : "Express gel nails",
              des : "",
              price : "25",
              pricesession : ""
            },
            {
              package : "Design from",
              des : "",
              price : "5",
              pricesession : ""
            },
            {
              package : "French tips extra",
              des : "",
              price : "10",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "WAXING SERVICE",
          service : [
            {
              package : "Eyebrows wax",
              des : "",
              price : "10",
              pricesession : ""
            },
            {
              package : "Upper lips wax",
              des : "",
              price : "10",
              pricesession : ""
            },
            {
              package : "Chin",
              des : "",
              price : "10",
              pricesession : ""
            },
            {
              package : "Full face",
              des : "",
              price : "30",
              pricesession : ""
            },
            {
              package : "Under arm",
              des : "",
              price : "15",
              pricesession : ""
            },
            {
              package : "Full leg",
              des : "",
              price : "55",
              pricesession : ""
            },
            {
              package : "Haft leg",
              des : "",
              price : "30",
              pricesession : ""
            },
            {
              package : "Full arm",
              des : "",
              price : "40",
              pricesession : ""
            },
            {
              package : "Haft arm",
              des : "",
              price : "25",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "TINTING SERVICE",
          service : [
            {
              package : "Lashes tint",
              des : "",
              price : "15",
              pricesession : ""
            },
            {
              package : "Eyebrows tint",
              des : "",
              price : "15",
              pricesession : ""
            },
            {
              package : "Eyebrows and lashes tint",
              des : "",
              price : "25",
              pricesession : ""
            },
            {
              package : "Eyebrows and lashes tint",
              des : "",
              price : "20",
              pricesession : ""
            }
          ]
        }
      ]
    },
    {
      title : "LASHES EXTENTIONS",
      service : [
        {
          titleservice : "NATURAL STYLE",
          service : [
            {
              package : "Nature classic lashes",
              des : "",
              price : "65",
              pricesession : ""
            },
            {
              package : "Full classic lashes",
              des : "",
              price : "70",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "PRE FAN MAKES LASHES",
          service : [
            {
              package : "Nature volume",
              des : "",
              price : "75",
              pricesession : ""
            },
            {
              package : "Hyubird style",
              des : "",
              price : "80",
              pricesession : ""
            },
            {
              package : "Mega volume",
              des : "",
              price : "85",
              pricesession : ""
            },
            {
              package : "Wispy style lashes",
              des : "",
              price : "90",
              pricesession : ""
            },
            {
              package : "Wet wispy style looks",
              des : "",
              price : "95",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "HAND MADE STYLE",
          service : [
            {
              package : "Nature volume",
              des : "",
              price : "80",
              pricesession : ""
            },
            {
              package : "Hyubird style",
              des : "",
              price : "90",
              pricesession : ""
            },
            {
              package : "Mega volume",
              des : "",
              price : "80",
              pricesession : ""
            },
            {
              package : "Other free style make anime lashes",
              des : "",
              price : "75",
              pricesession : ""
            }
          ]
        }
      ]
    },
    {
      title : "BROWS & LIPS TATTOOS",
      service : [
        {
          titleservice : "BROWS SERVICE",
          service : [
            {
              package : "Shading powder",
              des : "",
              price : "325 - 1st session",
              pricesession : "150 - 2nd session"
            },
            {
              package : "Hair straker",
              des : "",
              price : "650 - 1st session",
              pricesession : "150 - 2nd session"
            },
            {
              package : "Ombre powder brows",
              des : "",
              price : "399 - 1st session",
              pricesession : "150 - 2nd session"
            }
          ]
        },
        {
          titleservice : "LIPS TINTED",
          service : [
            {
              package : "Dark lips neutralisation",
              des : "",
              price : "399 - 1st session",
              pricesession : "175 - 2nd session"
            },
            {
              package : "Lips blushing",
              des : "",
              price : "350 - 1st session",
              pricesession : "150 - 2nd session"
            },
            {
              package : "Lips cover",
              des : "",
              price : "350 - 1st session",
              pricesession : "100 - 2nd session"
            },
            {
              package : "Lip colour removal",
              des : "Combo 1-3 times",
              price : "400",
              pricesession : ""
            },
            {
              package : "Eyeliner",
              des : "",
              price : "400",
              pricesession : ""
            }
          ]
        }
      ]
    },
    {
      title : "HAIR WASH AND MASSAGE",
      service : [
        {
          titleservice : "HAIR SERVICE",
          service : [
            {
              package : "Hair wash and head massage",
              des : "",
              price : "50",
              pricesession : ""
            },
            {
              package : "Hair wash and head + shoulder massage",
              des : "",
              price : "80",
              pricesession : ""
            },
            {
              package : "Hair & Face wash and clean",
              des : "",
              price : "50",
              pricesession : ""
            }
          ]
        }
      ]
    },
    {
      title : "OTHER SERVICE",
      service : [
        {
          titleservice : "LASHES LIFT",
          service : [
            {
              package : "Lashes lift and tint",
              des : "",
              price : "70",
              pricesession : ""
            },
            {
              package : "Lashes lift only",
              des : "",
              price : "60",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "LASHES REMOVAL",
          service : [
            {
              package : "Lashes removal",
              des : "",
              price : "25",
              pricesession : ""
            },
            {
              package : "Lashes removal and add on",
              des : "",
              price : "10",
              pricesession : ""
            },
            {
              package : "Second time",
              des : "",
              price : "Free",
              pricesession : ""
            }
          ]
        }
      ]
    },
    {
      title : "SEMI PERMANENT MAKE UP TATTOO TRANING 1:1",
      service : [
        {
          titleservice : "POWDER BROWS",
          service : [
            {
              package : "Basic course",
              des : "5 days",
              price : "2500",
              pricesession : ""
            },
            {
              package : "Advance course",
              des : "3 days",
              price : "3500",
              pricesession : ""
            },
            {
              package : "Basic to Advance course",
              des : "7 days",
              price : "5000",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "HAIRSTROKER COURSE",
          service : [
            {
              package : "Basic to Advance course",
              des : "7 days",
              price : "5500",
              pricesession : ""
            }
          ]
        },
        {
          titleservice : "SLIPS COURSE",
          service : [
            {
              package : "Basic course",
              des : "5 days",
              price : "2500",
              pricesession : ""
            },
            {
              package : "Advance course",
              des : "5 days",
              price : "5500",
              pricesession : ""
            },
            {
              package : "Basic to Advance course",
              des : "10 days",
              price : "7000",
              pricesession : ""
            },
            {
              package : "Colour course",
              des : "( how to use colour in PMU ) cover old eyebrows and lips, Use colour to treat dark lips, How to use green , blue , purple for lips",
              price : "1500",
              pricesession : ""
            }
          ]
        }
      ]
    }
  ]

export default function ListImage () {

    const dispatch = useAppDispatch()
    const bottomInView = useInView();
    const albumData = useAppSelector(state => state.albumSlice.albums);
    const translate = useAppSelector(state => state.translateSlice)
    const [t] = useTranslation();

    useEffect(()=> {
        dispatch(getAlbum())
    }, [])


    return (
        <div className="">
            <h2 className="lssm:text-px20 md:text-[40px] xl:text-[48px] uppercase text-text-primary text-center font-bold my-[95px] sc>768:my-[35px]">
                {t("library_image.title")}
            </h2>
            <div className="">
            <div className="w-full lssm:mb-[21px] md:mb-[147px]" >
            <div className="lssm:mt-[21px] md:mt-[40px] w-full mb-[35px]">
            <div className="grid grid-cols-12 gap-5">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-8 text-justify">
                    {t("library_image.faq.procedure-title")}
                    </pre>
                    <div className="flex flex-col items-start lssm:text-px14 md:text-px16 text-justify">
                    <p>{t("library_image.faq.procedure-1")}</p>
                    <p>{t("library_image.faq.procedure-2")}</p>
                    <p>{t("library_image.faq.procedure-3")}</p>
                    <p>{t("library_image.faq.procedure-4")}</p>
                    <p>{t("library_image.faq.procedure-5")}</p>
                    <p>{t("library_image.faq.procedure-6")}</p>
                    <p>{t("library_image.faq.procedure-7")}</p>
                    <p>{t("library_image.faq.procedure-8")}</p>
                    <p>{t("library_image.faq.procedure-9")}</p>
                    <p>{t("library_image.faq.procedure-10")}</p>
                    <p>{t("library_image.faq.procedure-11")}</p>
                    <p className="pb-8">{t("library_image.faq.procedure-12")}</p>
                    <p>{t("library_image.faq.procedure-13")}</p>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-1")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-8 text-justify">
                    {t("library_image.faq.procedure-14")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-2")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-15")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-16")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-17")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-18")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-3")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-19")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-4")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-20")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-5")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-21")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-6")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-22")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-7")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-23")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-24")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-8")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-25")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-26")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-27")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-28")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-29")}
                    </pre>
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-30")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-9")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-31")}
                    </pre>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-10")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-32")}
                    </pre>
                </div>
            </div><div className="grid grid-cols-12 gap-5 mt-12">
                <h3 className="xl:col-span-4 lssm:col-span-12 text-[#2F2D38] text-[20px] sc>768:text-center uppercase font-medium">
                {t("library_image.faq.procedure-title-11")}
                </h3>
                <div className="xl:col-span-8 lssm:col-span-12">
                    <pre className="whitespace-pre-wrap lssm:text-px14 md:text-px16 pb-5 text-justify">
                    {t("library_image.faq.procedure-33")}
                    </pre>
                </div>
            </div>

            <div className="text-center xl:my-14 lssm:my-10">
              <h2 className="Valky 2xl:mb-8 lssm:mb-0 text-[#F5E3D5] text-[48px] font-medium 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[38px] 2xl:text-[56px]">{t("home.topic.best")}</h2>
              <span className="Valky text-[#2F2D38] text-[48px] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]">{t("home.topic.caring")}</span>
            </div>
            <div className="grid grid-cols-12 2xl:my-16 xl:my-12 lg:my-8 max-lgs:my-8 gap-5">
            <div className="lg:col-span-7 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/members_bg.jpg" alt="" />
            </div>
            <div className="lg:col-span-5 sm-480:col-span-12 lsm-360:col-span-12 rounded-[18px]">
              <div className="grid grid-cols-12 gap-5">
                <div className="lg:col-span-12 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/members_bg1.jpg" alt="" />
                </div>
                <div className="lg:col-span-12 sm-480:col-span-3 lsm-360:col-span-12 rounded-[18px]">
                  <img className="w-full object-cover rounded-[18px]" src="https://kbeautyartistry.co.nz/fe/members_bg2.jpg" alt="" />
                </div>
              </div>
            </div>
          </div>
            {/* <div ref={bottomInView.ref}>
          <img className={clsx("w-full rounded-[10px]", {
              "animate__animated animate__pulse": bottomInView.isInView,
            })}
            src={`${hostBE}/fe/members_bg.jpg`}
            alt=""
          />
            </div> */}
            {/* <div className="grid grid-cols-12 gap-5 sm-480:px-[5%]">
              {data1.map((data, index) => {
                return <div key={index} className="2xl:col-span-4 m992:col-span-6 lssm:col-span-12 lg:my-10 lssm:my-6">
                <h3 className="bg-[#E9B495] px-4 py-3 text-white font-semibold text-center xl:text-2xl md:text-[18px] sm:text-2xl lssm:text-[16px] rounded-[30px] mb-14 w-[80%] mx-auto">{data.title}</h3>
                {data.service.map((dataservice, index) => {
                  return <div>
                <p className="xl:text-2xl md:text-[18px] sm:text-2xl lssm:text-[18px] text-[#727475] font-bold mb-5 mt-12">{dataservice.titleservice}</p>
                <ul key={index} className="pricing-list">
                  <li className="">
                    {dataservice.service.map((dataitem, index) => {
                      return <ul key={index} className="pricing-lists">
                          <li className="pricing-list-txt relative">
                            <div>
                              <h4 key={index} className="text-[#808080] md:text-[16px] lssm:text-[14px] font-bold mb-0">{dataitem.package}</h4>
                              <h4 key={index} className="text-[#808080] md:text-[14px] lssm:text-[12px] font-medium mb-0">{dataitem.des}</h4>
                            </div>
                            <div className="flex flex-col absolute right-[10px] top-0">
                              <h4 key={index} className="text-[#808080] md:text-[16px] lssm:text-[14px] font-bold mb-0">{dataitem.price}</h4>
                              <h4 key={index} className="text-[#808080] md:text-[16px] lssm:text-[14px] font-bold mb-0">{dataitem.pricesession}</h4>
                            </div>
                          </li>
                        </ul>
                    })}
                  </li>
                </ul>
                  </div>
                })}
              </div>
              })}
            </div> */}
          </div>
        </div>
            </div>        
        </div>

    )
}