import { hostBE } from "../../types/host";

export default function Banner() {


  return (
    <div className="h-auto max-h-max">
      <img 
       src={`${hostBE}/images/homepages/introfaq.png`}
      alt="banner"
      className="w-full banner_home_primary_images object-cover"
      />
    </div>
  );
}
