import { useState } from "react";
import { StepperContext } from "../../contexts/StepperContext";
import Button from "../../components/Button";
import Stepper from "../../components/Stepper/Stepper";
import StepperControl from "../../components/Stepper/StepperControl";
import Service from "../../components/steps/Service";
import { useNavigate } from "react-router-dom";
import Confirm from "../../components/steps/Confirm";
import TimeStaff from "../../components/steps/TimeStaff";
import Payment from "../../components/steps/Payment";
import Confirmed from "../../components/steps/Confirmed";
import { hostBE } from "../../types/host";

export default function FormContact() {
  const navigate = useNavigate();

  const [currentStep, setCurrentStep] = useState(1);
  const [userData, setUserData] = useState('');
  const [finalData, setFinalData] = useState({
    currentStep: 1,
    
    yieldBy: function (this:any, step:number, id?:number) {
      let newStep = id?id:this.currentStep;
      this.currentStep = (newStep <= steps.length)? newStep + step: newStep;
      setCurrentStep(this.currentStep); // for triggering update
    },
    callback: () => {return false;},


    backBool: true,
    location: {
      id: 0,
      name: "",
      address: ""
    },

    service: [],
    onHighLighted: [],

    staff: {
      id: 0,
      name: ""
    },
    bookingDate: (() => {const tmpDate = new Date(); tmpDate.setHours(0,0,0,0); return tmpDate})(),
    time: ""
  });

  const steps = [
    // "Location",
    "Service",
    "Time & Staff",
    "Complete",
    "Payment",
    "Confirmed"
  ];

  const icons = [
    // "A",
    "B",
    "C",
    "D",
    "E",
    "F"
  ];

  const handleClickExtra = (direction:any) => {
    if (!direction) {
      setCurrentStep(currentStep - 1)
      finalData.currentStep = currentStep;
      return 
    }
    setCurrentStep(currentStep + 1)
    finalData.currentStep = currentStep;
     
    // : (() => {console.log("DFJDKLFJDSFKLJDLKFJDKLSJ")})()
  }

  const displaySteps = (step: number) => {
    switch (step) {
      // case 1:
      //   return <Location data={finalData} setter={setFinalData} />
      case 1:
        return <Service data={finalData} setter={setFinalData}/>
      case 2:
        return <TimeStaff data={finalData} setter={setFinalData}/>
      case 3:
        return <Confirm data={finalData} setter={setFinalData}/>
      case 4:
        return <Payment data={finalData} setter={setFinalData} id={5}/>
      case 5:
        return <Confirmed />
      default:
    }
  }

  return (
    currentStep != 6 ? <div className="mt-36 mt-[24px] mb-[134px] mx-[74px] sc>768:mx-[30px]">
      {/* Stepper */}
      <div className="container horizontal relative pt-40">
        <Stepper
          steps={steps}
          currentStep={currentStep}
        />
        {/* Display Components */}

        <div className="mt-4 py-10">
          <StepperContext.Provider value={{
            userData,
            setUserData,
            finalData,
            setFinalData
          }}>
            {displaySteps(currentStep)}
          </StepperContext.Provider>
        </div>
      </div>

      {/* StepperControl */}
      {currentStep != steps.length &&
        <StepperControl
          handleClick={handleClickExtra}
          currentStep={currentStep}
          steps={steps}
        />
      }
    
    </div> : <div className="w-full h-full text-white px-[95px] py-72"
      style={{
      backgroundImage: `url(${hostBE}/fe/bookingdoja.png)`,
      backgroundRepeat: "no-repeat",
      backgroundSize : "cover"
    }}
    >
      <div className="text-center bg-[#b3a09759] rounded-[10px] px-[32px] py-[72px]">
        <h2 className="leading-[48px] text-[36px] sm>480:text-[14px] text-white">Thank you for your reservation. Your information has been submitted successfully!</h2>
      </div>
      <div className="text-center mt-[25px]">
        <Button
          onClick={() => {
            navigate("/lien-he")
            window.location.reload();
          }}
          color="primary"
          className="sc>768:text-px14 text-px16 uppercase 2xl:px-[165px] py-[10px] lg:px-[28px] max-w-fit shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)] rounded-[10px] m-auto"
        >
          Make another booking
        </Button>
      </div>
    </div>
  );

}