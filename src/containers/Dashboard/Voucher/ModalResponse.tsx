import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "../../../components/Button";
import Input from "../../../components/InputImage";
import Radio from "../../../components/Radio";
import { useAppDispatch, useAppSelector } from "../../../hooks/hook";
import { orderStatusSuccess } from "../../../reducers/vouchermanagerSlice";
import { hideModal } from "../../../reducers/modal";
import { typeVoucherEdit } from "../../../typeProps/Vouchertype";
import { pushPopup } from "../../../reducers/popupSlice";
import voucherService from "../../../services/voucher";
import { Voucher } from "../../../types";
import { boolean } from "yup";

type Props = {
    voucher: Voucher
}

export default function ModalResponse({ voucher }: Props) {
    const dispatch = useAppDispatch();
    const [orderStatus, setOrderStatus] = useState<boolean | undefined>(voucher.orderStatus);
    const [message, setMessage] = useState("");
    const [t] = useTranslation();
    const { isEnglish } = useAppSelector(state => state.translateSlice)

    const [voucherInput, setVoucherInput] = useState<typeVoucherEdit>({
        voucher: "",
        orderStatus: false,
        to: "",
        from: "",
        email: "",
        phoneNo: "",
        expirationDate: ""
    });

    const handleSelect = (event: ChangeEvent<HTMLSelectElement>) => {
        // const name = event.target.name;
        const value = event.target.value;
        setVoucherInput({
            ...voucherInput,
            orderStatus: value === "1" ? true : false,
        });
    };

    useEffect(() => { console.log(voucherInput.orderStatus) }, [voucherInput])

    const handleSubmit = (event: FormEvent) => {
        event.preventDefault();
        if (orderStatus === true) {
            setMessage("Voucher is used. Update unsuccessfully.")
        } else {
            voucherService.orderStatusContact({ id: voucher.id, orderStatus: voucherInput.orderStatus }).then((data: Voucher) => {
                dispatch(orderStatusSuccess(data));
                dispatch(hideModal())
                dispatch(pushPopup({
                    type: "SUCCESS",
                    message: t("notify.success", { name: t("contact.response") })
                }))
            }).catch(error => {
                dispatch(pushPopup({
                    type: "ERROR",
                    message: t("notify.error", { name: t("contact.response") })
                }))
            })
        }
    }

    return (
        <div className="lssm:w-[380px] sm:w-[500px] md:w-[600px] lg:w-[840px]  rounded-[20px] bg-white-color py-[30px] lssm:px-[24px] md:px-[40px] xl:px-[80px]">
            <h2 className="lssm:text-px20 md:text-[32px] text-text-primary font-bold text-center mb-[50px]">{t("dashboard.request.form.titlevoucher")}</h2>

            <form onSubmit={handleSubmit} className="grid lssm:grid-cols-1 md:grid-cols-2 gap-x-[24px] gap-y-[30px]">
                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.to")}</label>
                    <Input value={voucher.to} readOnly={true} />
                </div>

                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.from")}</label>
                    <Input value={voucher.from} readOnly={true} />
                </div>

                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.amount")}</label>
                    <Input value={voucher?.voucher?.name} readOnly={true} />
                </div>

                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.expires")}</label>
                    <Input value={voucher.expirationDate} readOnly={true} />
                </div>

                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.phone")}</label>
                    <Input value={voucher.phoneNo} readOnly={true} />
                </div>

                <div>
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.email")}</label>
                    <Input value={voucher.email} readOnly={true} />
                </div>

                <div className="md:col-span-2">
                    <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.status")}</label>
                    <select
                        value={voucherInput.orderStatus ? "1" : "0"}
                        name="orderStatus"
                        onChange={(event) => {
                            setVoucherInput(prev => { return { ...prev, orderStatus: event.target.value == "0" ? false : true } })
                        }}
                        className="w-full px-5 py-3 sm-480:text-base text-sm text-primary focus:outline-none border border-border-gray rounded-md"
                    >
                        <option value="1">
                            Used
                        </option>
                        <option value="0">
                            Not used
                        </option>
                    </select>
                    <small className="mt-2 text-[#FF0000 text-px14]">{message}</small>
                </div>
                <div className="md:col-span-2 flex justify-end">
                    <Button onClick={() => dispatch(hideModal())} color="empty" className="h-[60px] w-[120px]">{t("button.cancel")}</Button>
                    <Button type="submit" color="primary" className="h-[60px] ml-3 w-[120px]">{t("dashboard.request.form.btn_response")}</Button>
                </div>
            </form>

        </div>
    )
}