import Button from "../../components/Button";
import Input from "../../components/CareerInput";
import {useFormik} from "formik";
import * as Yup from "yup";
import { useAppDispatch } from "../../hooks/hook";
import { createUser } from "../../reducers/user";
import { User } from "../../types";
import { ChangeEvent, useEffect, useState } from "react";
import userService from "../../services/user";
import { hideModal } from "../../reducers/modal";



export default function ModalCreateUser() {

    const dispatch = useAppDispatch();
    const [isSubmit, setSubmit] = useState(true);

    const formik = useFormik({
        initialValues: {
            fullname: "",
            gender: "true",
            birth: "",
            position: "",
            // email: "",
            phone: "",
            password: "",
            login: ""
        },       
        validationSchema: Yup.object({
            fullname: Yup.string().required("Bắt buộc").min(5, "Tên người dùng tối thiểu 5 ký tự"),
            birth: Yup.string().required("Bắt buộc"),
            // email: Yup.string().required("Bắt buộc").matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/, "Đây không phải địa chỉ email"),
            phone: Yup.string().required("Bắt buộc").matches(/^[0-9]{10}$/, "Vui lòng nhập số điện thoại"),
            position: Yup.string().required("Bắt buộc").min(3, "Vị trí tối thiểu 3 ký tự"),
            login: Yup.string().required("Bắt buộc").min(6, "Tên người dùng tối thiểu 6 ký tự"),
            password: Yup.string().required("Bắt buộc").matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/, "Mật khẩu phải có ít nhất 1 chữ hoa, 1 ký tự đặc biệt, số").min(8, "Tên người dùng tối thiểu 8 ký tự"),
        }),
        onSubmit: (values)=> {
            if(isSubmit) {
                dispatch(createUser({
                    ...values,
                    gender: values.gender === "true" ? true : false
                }))  
                
            }
        },  
      
        
    })

    const handleValidateEmail = (event:ChangeEvent<HTMLInputElement>) => {
        userService.validateEmail(event.target.value.trim()).then(result => {
                if(!result) {
                    formik.setFieldError("email", "Email này đã tồn tại") 
                }
                setSubmit(result)                          
        })
    }

    const handleValidateLogin = (event:ChangeEvent<HTMLInputElement>)=> {
        userService.validateLogin(event.target.value.trim()).then(result => {
            if(!result) {
                formik.setFieldError("login", "Tên đăng nhập này đã tồn tại")
                    
            }
            setSubmit(result)                          

    })
    }



    return <div className="lssm:w-[100vw] md:w-[900px] bg-white-color p-[50px] lssm:translate-y-[25%] md:translate-y-0">
                <h3 className="text-[32px] uppercase font-medium text-text-primary mb-[30px] text-center">THÊM THÀNH VIÊN</h3>

                <form onSubmit={formik.handleSubmit} className="grid lssm:grid-cols-1 md:grid-cols-2 gap-5">
                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Tên người dùng <span className="text-[#EB0000]">*</span></label>
                            <Input name="fullname" value={formik.values.fullname} onChange={formik.handleChange} />
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.fullname}</small>
                        </div>

                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Giới tính <span className="text-[#EB0000]">*</span></label>
                            <select name="gender" value={formik.values.gender} onChange={formik.handleChange} className="border-[1px] px-1 border-solid border-text-primary outline-none  rounded-[10px] w-full h-[50px]">
                                        <option value="true">nam</option>
                                        <option value="false">nữ</option>
                            </select>
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.gender}</small>

                        </div>

                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Năm sinh <span className="text-[#EB0000]">*</span></label>
                            <Input name="birth" type="date" value={formik.values.birth} onChange={formik.handleChange} />
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.birth}</small>

                        </div>

                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Chức vụ <span className="text-[#EB0000]">*</span></label>
                            <Input name="position" value={formik.values.position} onChange={formik.handleChange} />
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.position}</small>

                        </div>

                        {/* <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Email <span className="text-[#EB0000]">*</span></label>
                            <Input onBlur={handleValidateEmail} name="email" type="email" value={formik.values.email} onChange={formik.handleChange}  />
                            {
                                formik.touched.email && formik.errors.email && (

                                    <small className="mt-1 text-px12 text-error_color">{formik.errors.email}</small>
                                )
                            }

                        </div> */}

                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Số điện thoại <span className="text-[#EB0000]">*</span></label>
                            <Input name="phone" type="text" value={formik.values.phone} onChange={formik.handleChange} />
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.phone}</small>

                        </div>
                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Tên đăng nhập <span className="text-[#EB0000]">*</span></label>
                            <Input onBlur={handleValidateLogin} name="login" type="text" value={formik.values.login} onChange={formik.handleChange} />
                            {
                                formik.touched.login && formik.errors.login && (

                                    <small className="mt-1 text-px12 text-error_color">{formik.errors.login}</small>
                                )
                            }

                        </div>

                        <div>
                            <label className="text-px16 font-medium text-text-primary mb-3 block">Mật khẩu <span className="text-[#EB0000]">*</span></label>
                            <Input name="password" type="text" value={formik.values.password} onChange={formik.handleChange} />
                            <small className="mt-1 text-px12 text-error_color">{formik.errors.password}</small>

                        </div>
                    <div className="col-span-2 sc>768:col-span-1 flex justify-end">
                        <Button onClick={()=> dispatch(hideModal())} type="button" color="empty" className="border-[#EB3B3B] text-[#F20E0E]">Hủy</Button>
                        <Button type="submit" color="primary" className="ml-3">Thêm</Button>
                    </div>
                      
                </form>
    </div>
}