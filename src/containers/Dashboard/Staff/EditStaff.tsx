
import Button from "../../../components/Button"
import Input from "../../../components/InputImage"
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import {Navigation} from "swiper";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup"
import uploadService from "../../../services/uploadImage";
import { useAppDispatch } from "../../../hooks/hook";
import StaffManager from "../../../services/staff";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Staff } from "../../../types";
import { pushPopup } from "../../../reducers/popupSlice";
import { FaBriefcase, FaMoneyBill, FaUsers } from "react-icons/fa";
import { RiMedalFill } from "react-icons/ri";
import { MdAddPhotoAlternate, MdOutlineTransgender } from "react-icons/md";
import { hostBE } from "../../../types/host";
import { useTranslation } from "react-i18next";

export default function EditStaff() {
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    const param = useParams();
    const navigate = useNavigate();
    const [staff, setRecruit] = useState<Staff>();
    const [t] = useTranslation();

    const [file, setFile] = useState<File | null>(null)
    const [urlImagePreview, setUrlPreview] = useState<string>("");
    const dispatch = useAppDispatch();

    const formik = useFormik({
        initialValues: {
            name: "",
            birthday: "",
            email: "",
            phoneNo: "",
            description: "",
            address: ""
        },
        validationSchema: Yup.object({
            name: Yup.string().required(t("validate.error.required")),
            birthday: Yup.string().required(t("validate.error.required")),
            email: Yup.string().required(t("validate.error.required")),
            phoneNo: Yup.string().required(t("validate.error.required")),
            description: Yup.string().required(t("validate.error.required")),
            address: Yup.string().required(t("validate.error.required"))
        }) ,
        onSubmit: async (values) => {
            const formData = new FormData();
            let fileUrl;
            if(file) {
                formData.append("file", file);
                 fileUrl = await uploadService.upload(formData)

            }

            StaffManager.editStaff({
                ...values,
                image: fileUrl?.list[0].image_url ??  staff?.image + "",
                id: parseInt(param.idEdit+"")
            }).then((res:Staff) => {
                navigate("/quanly/nhanvien")
                dispatch(pushPopup({
                    type: "SUCCESS",
                    message: t("notify.success", {name: t("notify.update")})
                }))
            }).catch(()=> {
                dispatch(pushPopup({
                    type: "SUCCESS",
                    message: t("notify.error", {name: t("notify.update")})
                }))
            })
        }
    })

    useEffect(()=> {
        if(param.idView || param.idEdit) {
            const id:any = param.idView ?? param.idEdit
         
            StaffManager.getById(id).then((data:Staff) => {
                const dateString = new Date(data.birthday);
                const date = dateString.setDate(dateString.getDate() + 3); 
                const defaultValue = new Date(date).toISOString().split('T')[0]
                setRecruit(data);
                    formik.setValues({
                        name: data.name,
                        email: data.email,
                        birthday: defaultValue,
                        phoneNo: data.phoneNo,
                        description: data.description,
                        address: data.address
                    })
                    setUrlPreview(data.image)
            })

        }

    }, [param])

    const handleChangeFile = (event:ChangeEvent<HTMLInputElement>) => {
        const file = event.currentTarget.files![0]
        if(file.size < 26675200) {
            setFile(file)
            setUrlPreview(URL.createObjectURL(file));
        }else {
            dispatch(pushPopup({
                message: t("notify.min_image", {size: "25MB"}),
                type: "WARNING"
            }))
        }
    }

    const navigateEdit = () => {
        navigate(`/quanly/nhanvien/edit/${param.idView}`)
    }

    const onInputClick = (e:React.MouseEvent<HTMLInputElement, MouseEvent>) => {
        e.currentTarget.value = ""
    }

    const slideToTop = () => {
        window.scrollBy(0, -document.documentElement.scrollTop);
    }

    return <form onSubmit={formik.handleSubmit}>
                        
                             <div className="w-full">
                                <div>
                                <h2 className="uppercase text-[48px] sc>768:text-px20 text-text-primary font-bold text-center py-[74px] lssm:pb-[32px] md:pb-[117px]">{param.idView ? t("dashboard.recruitment.detail.titlestaff") : t("dashboard.recruitment.edit.titlestaff") }</h2>

                                    <div className=" mb-[80px] flex">
                                        <div className="grid lssm:grid-cols-1 md:grid-cols-2 gap-x-5 w-full">
                                        <div className="md:col-span-2">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.title_staff")}</label>
                                          <Input readOnly={param.idView ? true : false} value={formik.values.name} onChange={formik.handleChange} name="name" />
                                          {formik.errors.name &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.name}</small>}
                                        </div>

                                        <div className="md:col-span-2 mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.upload_staff")}:</label>
                                            <div className="border border-solid border-border-color p-[20px] grid lssm:grid-cols-1 md:grid-cols-2 outline-none rounded-[5px]">
                                                {
                                                    !param.idView && (
                                                    <div className="">
                                                            <p className="text-[#636363] mb-[30px] lssm:text-px14 md:text-px16 font-medium">{t("dashboard.recruitment.create.form.update_format")}</p>
                                                    
                                                        <label htmlFor='career-banner' className="lssm:w-full md:w-[220px] h-[120px] border-2 border-dashed border-primary text-text-primary flex flex-col items-center justify-center rounded-[5px]">
                                                             <span className="text-primary lssm:text-[32px] md:text-[48px]"><MdAddPhotoAlternate /></span>
                                                            <span className="mt-[32px]">{t("dashboard.recruitment.create.form.upload_i")}</span>
                                                            <input onChange={handleChangeFile} onClick={onInputClick} hidden type="file" id="career-banner" />
                                                        </label>
                                                    </div>
                                                        
                                                    )
                                                    
                                                 }


                                                    <div className="">
                                                            <p className="text-[#636363] mb-[30px] lssm:text-px14 md:text-px16 font-medium">{t("dashboard.recruitment.create.form.preview_i")}:</p>
                                                    
                                                        <div  className="lssm:w-full md:w-[220px] h-[120px] bg-[#6363631a] border-primary text-text-primary flex flex-col items-center justify-center rounded-[5px] overflow-hidden">
                                                            {
                                                                urlImagePreview !== "" ? (
                                                                    <img src={urlImagePreview} alt="" className="w-full h-full object-contain" />
                                                                ): (
                                                                    <img src={`${hostBE}/fe/previewImage.png`} alt="" />
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.date_staff")}</label>
                                            <Input readOnly={param.idView ? true : false} value={formik.values.birthday} onChange={formik.handleChange} name="birthday" type="date"  />
                                        {formik.errors.birthday &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.birthday}</small>}

                                        </div>  

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.email_staff")}</label>
                                        <Input readOnly={param.idView ? true : false} value={formik.values.email} name="email" onChange={formik.handleChange} label={<FaMoneyBill/> } />
                                        {formik.errors.email &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.email}</small>}

                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.number_phone")}</label>
                                            <Input readOnly={param.idView ? true : false} value={formik.values.phoneNo} name="phoneNo" onChange={formik.handleChange} label={<FaUsers />} />
                                        {formik.errors.phoneNo &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.phoneNo}</small>}

                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.adress_staff")}</label>
                                        <Input readOnly={param.idView ? true : false} value={formik.values.address} name="address" onChange={formik.handleChange}  label={<FaBriefcase />} />
                                        {formik.errors.address &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.address}</small>}

                                        </div>

                                        <div className="mt-[32px] md:col-span-2">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">{t("dashboard.recruitment.create.form.description_staff")}</label>
                                        <textarea readOnly={param.idView ? true : false} value={formik.values.description} name="description" onChange={formik.handleChange} className="w-full p-2 lssm:h-[400px] md:h-[500px] border border-solid border-border-color focus-within:border-blue-500 outline-none rounded-[5px]">

                                        </textarea>
                                        {formik.errors.description &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.description}</small>}

                                        </div>

                                        <div className="md:col-span-2 flex justify-end mt-[32px]">
                                         
                                        {
                                                param.idView ? (
                                                <div className="md:col-span-2 flex justify-end mt-[32px]">

                                                    <Link to="/quanly/nhanvien" className="sc>768:flex-1 md:w-[165px]  flex justify-center items-center border-[1px] border-[#8C6659] lssm:text-px14 md:text-px16 rounded-[5px] mr-3 h-[50px]" >
                                                    Go back
                                                    </Link>

                                                    <Button onClick={navigateEdit} type="button" color="primary" className="h-[50px] lssm:text-px14 md:text-px16 w-[293px] sc>768:flex-1">{t("dashboard.recruitment.detail.btn_update")}</Button>
                                                </div>
                                                ): (
                                                    <Button onClick={()=>navigate(-1)} color="empty" className="sc>768:flex-1 sc>768:text-px14 mr-3">{t("button.button_prev")}</Button>
                                                    )
                                                }

                                            {
                                                param.idEdit && (
                                                    <Button type="submit" color="primary" className="sc>768:flex-1 sc>768:text-px14 md:w-[292px] h-[50px]">{t("dashboard.recruitment.edit.btn_save")}</Button>

                                                )
                                            }
                                        </div>

                                    </div>
                                </div>
                                </div>
                                </div>
         </form>
}