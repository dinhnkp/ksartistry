
import Button from "../../../components/Button"
import Input from "../../../components/InputImage"
import clsx from "clsx"
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import {Navigation, Thumbs, Zoom} from "swiper";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup"
import uploadService from "../../../services/uploadImage";
import { useAppDispatch } from "../../../hooks/hook";
import { createStaffSuccess } from "../../../reducers/steff";
import { FaBriefcase, FaMoneyBill, FaUsers } from "react-icons/fa";
import { RiMedalFill } from "react-icons/ri";
import { MdAddPhotoAlternate, MdOutlineImage, MdOutlineTransgender } from "react-icons/md";
import { Link, useNavigate } from "react-router-dom";
import StaffManager from "../../../services/staff";
import { pushPopup } from "../../../reducers/popupSlice";
import { useTranslation } from "react-i18next";

export default function CreateStaff() {
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)

    const [file, setFile] = useState<File | null>(null)
    const [urlImagePreview, setUrlPreview] = useState<string>("");
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [t] = useTranslation();


    const formik = useFormik({
        initialValues: {
            name: "",
            birthday: "",
            email: "",
            phoneNo: "",
            description: "",
            address: ""
        },
        validationSchema: Yup.object({
            name: Yup.string().required(t("validate.error.required")),
            birthday: Yup.string().required(t("validate.error.required")),
            email: Yup.string().required(t("validate.error.required")),
            phoneNo: Yup.string().required(t("validate.error.required")),
            description: Yup.string().required(t("validate.error.required")),
            address: Yup.string().required(t("validate.error.required"))
        }) ,
        onSubmit: async (values) => {
            
            const formData = new FormData();
            
            let fileUrl;
            if(file) {
                formData.append("file", file);
                 fileUrl = await uploadService.upload(formData)

            }
            console.log({
                ...values,
                image: fileUrl?.list[0].image_url ?? ""
            });
            
            StaffManager.postStaff({
                ...values,
                image: fileUrl?.list[0].image_url ?? ""
            }).then((data) => {
                
                dispatch(createStaffSuccess(data))
                dispatch(pushPopup({
                    type: "SUCCESS",
                    message: t("notify.success", {name: t("button.add")})
                }));
                navigate(-1);
            }).catch(() => {

                    dispatch(pushPopup({
                        type: "ERROR",
                        message: t("notify.error", {name:  t("button.add")})
                    }));
            })

            

        }
    })



    const handleChangeFile = (event:ChangeEvent<HTMLInputElement>) => {
        const file = event.currentTarget.files![0]
        if(file.size < 26675200) {
            setFile(file)
            setUrlPreview(URL.createObjectURL(file));
            
        }else {
            dispatch(pushPopup({
                message: t("notify.min_image", {size: "25MB"}),
                type: "WARNING"
            }))
        }
    }

    const slideToTop = () => {
        window.scrollBy(0, -document.documentElement.scrollTop);
    }

    const onInputClick = (e:React.MouseEvent<HTMLInputElement, MouseEvent>) => {
        e.currentTarget.value = ""
    }

    return <form onSubmit={formik.handleSubmit}>                   
                             
                             <div className="w-full">
                                <div>
                                <h2 className="uppercase text-[48px] sc>768:text-px20 text-text-primary font-medium text-center py-[74px] lssm:pb-[32px] md:pb-[117px]">CREATE NEW STAFF</h2>

                                    <div className=" mb-[80px] flex">
                                        <div className="grid lssm:grid-cols-1 md:grid-cols-2 gap-x-5 w-full">
                                        <div className="md:col-span-2">
                                            <label className="lssm:text-px14 md:text-px20 text-text-primary font-bold mb-4 block">Name Staff :</label>
                                          <Input value={formik.values.name} onChange={formik.handleChange} name="name" />
                                          {formik.errors.name &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.name}</small> }
                                        </div>

                                        <div className="md:col-span-2 mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Upload picture staff :</label>
                                            <div className="border border-solid border-border-color p-[20px] grid lssm:grid-cols-1 md:grid-cols-2 outline-none rounded-[5px]">
                                                    <div className="">
                                                            <p className="text-[#636363] mb-[30px] lssm:text-px14 md:text-px16 font-medium">{t("dashboard.recruitment.create.form.update_format")}</p>
                                                    
                                                        <label htmlFor='career-banner' className="sc>768:mt-2 lssm:w-full md:w-[220px] h-[120px] border-2 border-dashed border-primary text-text-primary flex flex-col items-center justify-center rounded-[5px]">
                                                            <span className="text-primary text-[48px]"><MdAddPhotoAlternate /></span>
                                                            <span className="mt-[32px]">{t("dashboard.recruitment.create.form.upload_i")}</span>
                                                            <input onChange={handleChangeFile} onClick={onInputClick} name="image" hidden type="file" id="career-banner" />
                                                        </label>
                                                    </div>


                                                    <div className="sc>768:mt-[24px]">
                                                            <p className="text-[#636363] mb-[30px] lssm:text-px14 md:text-px16 font-medium">{t("dashboard.recruitment.create.form.preview_i")}:</p>
                                                    
                                                        <div  className="lssm:w-full md:w-[220px] h-[120px] bg-[#6363631a] border-primary text-text-primary flex flex-col items-center justify-center rounded-[5px] overflow-hidden">
                                                            {
                                                                urlImagePreview !== "" ? (
                                                                    <img src={urlImagePreview} alt="" className="w-full h-full object-cover" />
                                                                ): (
                                                                    <span className="text-[#DADADA] text-[48px]"><MdOutlineImage /></span>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Date of birth :</label>
                                            <Input value={formik.values.birthday} onChange={formik.handleChange} name="birthday" type="date"  />
                                        {formik.errors.birthday &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.birthday}</small>}

                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Email :</label>
                                        <Input value={formik.values.email} name="email" onChange={formik.handleChange} label={<FaMoneyBill />}/>
                                        {formik.errors.email &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.email}</small>}

                                        </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Phone Number :</label>
                                            <Input value={formik.values.phoneNo} name="phoneNo" onChange={formik.handleChange} label={<FaUsers />} />
                                               {formik.errors.phoneNo &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.phoneNo}</small>}

                                            </div>

                                        <div className="mt-[32px]">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Address :</label>
                                        <Input value={formik.values.address} name="address" onChange={formik.handleChange}  label={<FaBriefcase />}/>
                                        {formik.errors.address &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.address}</small>}

                                        </div>

                                        <div className="mt-[32px] md:col-span-2">
                                            <label className="lssm:text-px14 md:text-px20 font-bold text-text-primary mb-4 block">Description :</label>
                                        <textarea value={formik.values.description} name="description" onChange={formik.handleChange} className="w-full p-2 lssm:h-[400px] md:h-[500px] border border-solid border-border-color focus-within:border-blue-500 outline-none rounded-[5px]">

                                        </textarea>
                                        {formik.errors.description &&  <small className="mt-1 text-px12 text-error_color">{formik.errors.description}</small>}

                                        </div>

                                        <div className="md:col-span-2 flex justify-end mt-[32px]">
                                        <Link to="/quanly/nhanvien" className="sc>768:flex-1 md:w-[165px]  flex justify-center items-center border-[1px] border-[#8C6659] lssm:text-px14 md:text-px16 rounded-[5px] mr-3 h-[50px]" >
                                                    Go back
                                                    </Link>
                                         <Button type="submit" color="primary" className="sc>768:flex-1 md:w-[292px] sc>768:text-px14">{t("dashboard.recruitment.create.form.savestaff")}</Button>
                                     </div>

                                    </div>
                                </div>
                                </div>
                             </div>           
                        </form>
}