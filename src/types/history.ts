export type HistoryState = {
    id?:number
    imagePath?:string
    year?:string
    imageUrl?:string
    descriptionEn?: string
    descriptionVi?: string
}