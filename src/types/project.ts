

export type Project = {   
        id:number,
        titleEn: string,
        descriptionEn: string,
        contentEn: string,
        avatarUrl: string,
        avatarPath: string,
        createdDate: string
}