import { Location } from "./location"
import { Staff } from "./staff"
import { Package } from "./package"

export type Booking = {
    id?: number
    address: Location
    servicePackage: Package[]
    staff: Staff
    timetable: string
    bookingDate: string
    fullName: string
    email: string
    phoneNo: string
    orderId: string
}