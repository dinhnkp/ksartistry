export type UrlResponsive = {
    total: number,
    list: {
        image_path: string,
        image_url: string
    }[],
}


export type UploadFile = {
    fileUrl: string
    filePath: ""
}