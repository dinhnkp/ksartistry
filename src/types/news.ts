export type News = {
    id?: number
    titleVi: string
    titleEn: string
    descriptionVi: string
    descriptionEn: string
    contentVi: string
    contentEn: string
    avatarUrl: string
    avatarPath: string
    createdBy?: string,
  createdDate?: string
}