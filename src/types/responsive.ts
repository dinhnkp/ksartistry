export type ResponsiveData<T> = {
    total: number
    list: T[]
}

export type Option = {
    page: number
    limit: number
}