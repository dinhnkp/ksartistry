import { Amount } from "./amount"

export type Voucher = {
    id?: number
    voucher: Amount
    orderStatus?: boolean
    to: string
    from: string
    email: string
    phoneNo: string
    expirationDate: string
}
