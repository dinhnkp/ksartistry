import { TopicImage } from "./album"

export type Device ={
    id?: number,
    descriptionVi: string,
    descriptionEn: string,
    topicImage: TopicImage
  }


