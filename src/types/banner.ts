export type Banner = {
    id?: number,
    imageUrl: string
    imagePath: string
}

