import clsx from "clsx";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { GrFormDown } from "react-icons/gr";
import { Link, NavLink, useLocation } from "react-router-dom";
import { useAppSelector } from "../hooks/hook";
import { navList } from "../Router/NavList";

function MenuRows() {
  const location = useLocation();
  const { t } = useTranslation();
  const user = useAppSelector((state) => state.userSlice.currentUser);

  const getPathName = useCallback(
    (listPath: any[] | string) => {
      let isCheck = false;
      if(typeof listPath === "string") {
        if(location.pathname === "/" && listPath === "/") {{
          isCheck = true
        }}else {
          isCheck = listPath === "/" ? false : location.pathname.includes(listPath);

        }
      }else {

        const newArr = listPath.map((i) => i.path);
  
         isCheck = newArr.some((item) => item === location.pathname);
  
      }
      return isCheck;
    },
    [location.pathname]
  );
  return (
    <ul className="flex items-center">
      {navList.map((item, index) => {
        return (
          item.path !== "/quanly/thongtintaikhoan" ? (
            <li key={index} tabIndex={index + 2} className="mx-1 block w-max">
              <NavLink
                to={item.path}
                className={clsx("w-max px-3 text-[#4C1410]  hover:bg-[#F5ECE5] rounded-[12px] font-medium py-2 uppercase text-base w-1920:text-px16 2xl:text-px16 xl:text-[13px] md:text-px16 ", {"bg-[#F5ECE5]":getPathName(item.path)}) }
              >
                {t(`${item.name}`)}
              </NavLink>
            </li>
          ) : (
            user && (
              <li key={index} className=" mx-1  w-max">
                <NavLink
                  to={item.path}
                  className={clsx("px-3 w-max text-[#4C1410] rounded-[12px] hover:bg-[#F5ECE5]  font-medium py-2 w-1920:text-px16 2xl:text-px16 xl:text-[13px] md:text-px16 text-base uppercase ", {"bg-[#F5ECE5]":getPathName(item.path)}) }
                >
                  {t(`${item.name}`)}
                </NavLink>
              </li>
            )
          ) )
      })}
    </ul>
  );
}

export default MenuRows;
