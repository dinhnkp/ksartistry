import HttpService from "../configs/api";
import getApi from "./getApi";
import { Option, Amount, ResponsiveData } from "../types";
import { amountResult, amountTypePost, amountType, searchAmountParam } from "../typeProps/Amounttype";

const AmountManager = {
    get: (option:Option):Promise<ResponsiveData<Amount>> => {
        const url = getApi("voucher");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}})
    },
    getAmountHome: (option:Option):Promise<ResponsiveData<Amount>> => {
        const url = getApi("voucher/home");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    },
    postAmount: (data:amountTypePost):Promise<amountType> => {
        const url = getApi(`voucher`);
        return HttpService.axiosClient.post(url, data )
    },
    getById: (id:number):Promise<Amount> => {
        const url = getApi("voucher/"+id);
        return HttpService.axiosClient.get(url);
    },
    deleteAmount:(id:number):Promise<boolean>=> {
        const url = getApi(`voucher/${id}`);
        return HttpService.axiosClient.delete(url)
    },
    editAmount: (data: any):Promise<Amount> => {
        const url = getApi(`voucher`);
        return HttpService.axiosClient.put(url, data)
    },
    searchAmount:(data: searchAmountParam):Promise<amountResult> => { 
        const url = getApi(`voucher/search`);
        return HttpService.axiosClient.get(`${url}/${data.type}/${data.keySearch}`,{params: {page: data.option.page, limit: data.option.limit}});
    },
}

export default AmountManager