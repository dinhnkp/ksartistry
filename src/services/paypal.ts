import HttpService from "../configs/api"
import getApi from "./getApi"


const paypalService = {

    getTokenIdentity: () => {
        const url = getApi("paypal/token-identity");
        return HttpService.axiosClient.post(url);
    }
    // create: (banner: Banner[]):Promise<ResponsiveData<Banner>> => {
    //     const url = getApi("partner")
    //         return HttpService.axiosClient.post(url, banner);
    // },
    // find: ():Promise<Banner[]> => {
    //     const url = getApi("partner")
    //     return HttpService.axiosClient.get(url)
    // },
    // delete: (id:number):Promise<boolean> => {
    //     const url = getApi(`partner/image/${id}`);

    //     return HttpService.axiosClient.delete(url)


    // }
}


export default paypalService