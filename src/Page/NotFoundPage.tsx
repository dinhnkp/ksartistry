import React from "react";

function NotFoundPage() {
  return <div className="m-auto text-center mt-[50px] font-bold text-[32px] text-text-lightred uppercase">404 notfound</div>;
}

export default NotFoundPage;
