import clsx from "clsx";
import { ChangeEvent, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSearchParams } from "react-router-dom";
import DialogConfirm from "../../components/DialogConfirm";
import Pagination from "../../components/Pagination";
import ModalResponse from "../../containers/Dashboard/Voucher/ModalResponse";
import { AiOutlineSearch } from "react-icons/ai";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { deleteVoucherReduce, getVoucher, searchVoucher, setCurrentPage } from "../../reducers/vouchermanagerSlice";
import { showModal } from "../../reducers/modal";
import voucherService from "../../services/voucher";
import { hostBE } from "../../types/host";

const LIMIT = 9;

export default function Voucher() {

    const dispatch = useAppDispatch();

    const voucherData = useAppSelector(state => state.voucherSlice)
    const [value, setValue] = useState<string>();
    const [searchParam, setSearchParam] = useSearchParams();
    const [t] = useTranslation();
    const {isEnglish} = useAppSelector(state => state.translateSlice)    

    useEffect(()=> {
        if(value === undefined || value.trim() === "") {
            
            dispatch(getVoucher({page: voucherData.currentPage, limit: LIMIT}))

        }else {
            dispatch(searchVoucher({keyword: value, option: {page: voucherData.currentPage, limit: LIMIT}}));
        }
    }, [voucherData.currentPage])

    useEffect(() => {
        if(value !== undefined) {
            dispatch(searchVoucher({keyword: value, option: {page: 1, limit: LIMIT}}));
            setSearchParam({page: "1"});
        }
    }, [value])


    const showModalResponse = (id:string) => {
        voucherService.getVoucherById(id).then((data)=> {
            dispatch(showModal(<ModalResponse voucher={data} />))

        })
    }

    const showDialogConfirm = (id:string) => {
        dispatch(showModal(<DialogConfirm message="Do you want to delete it?" onClick={()=>deleteVoucher(id)} />))
    }

    const deleteVoucher = (id:string) => {
        dispatch(deleteVoucherReduce(parseInt(id)));
    }

    const total = useMemo(()=> {
        return Math.ceil(voucherData.total/LIMIT);
    }, [voucherData.total])

    const handleChange = (event:ChangeEvent<HTMLInputElement>) => {
        setValue(event.currentTarget.value);
    }

    return (
        <div>
             <h2 className="text-center text-text-primary lssm:text-px20 md:text-[48px] font-bold mt-[74px] mb-[48px] uppercase">{t("dashboard.request.title_voucher")}</h2>

             <div className="w-full h-[60px] border border-solid border-border-color focus-within:border-primary flex items-center rounded-[5px] overflow-hidden">
             <div className="w-[60px] h-full flex items-center justify-center border-r border-r-border-gray">
                <AiOutlineSearch className="text-bg_blue_bold text-3xl" />
            </div>
                <input value={value ?? ""} onChange={handleChange} className="flex-1 h-full px-2  outline-none border-none" placeholder={"Search..."} />
                
            </div>
            {
            voucherData.voucherList.length > 0 ? (
                <div>
                <div className="mt-[56px] border-2 border-solid border-border-color rounded-[12px] overflow-hidden mb-[60px] overflow-scroll">
                    <table className="dashboard-table w-full">
                        <thead>
                        <tr>
                    
                            <td></td>
                            <td className="whitespace-nowrap">{t("dashboard.request.to")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.from")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.amount")}</td>
                            {/* <td>{t("dashboard.request.expires")}</td> */}
                            <td className="whitespace-nowrap">{t("dashboard.request.email")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.phonenumber")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.status")}</td>
                        </tr>

                        </thead>
                        <tbody>
                        {
                            voucherData.voucherList.map((voucher)=> {
                                return (
                            <tr key={voucher.id} className="cursor-pointer" >
                                    <td>
                                        <div className="flex justify-center items-center relative z-[2]">
                                        <span className="cursor-pointer" onClick={()=> showDialogConfirm(voucher.id+"")}><img src={`${hostBE}/fe/delete_icon.png`} alt="" /></span>
                                        </div>
                                    </td>
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher.to}</td>
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher.from}</td>
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher?.voucher?.name}</td>
                                    {/* <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher.expirationDate}</td> */}
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher.email}</td>
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>{voucher.phoneNo}</td>
                                    <td onClick={()=> showModalResponse(voucher.id+"")}>
                                        <span className={clsx({"text-[#5CD931]":voucher.orderStatus, "text-[#F14040]":!voucher.orderStatus})}>{voucher.orderStatus ? ("Used") : ("Not used")}</span>
                                    </td>
                                    
                            </tr> 
                                )
                            })
                        }
                                

                        </tbody>
                    </table>

                </div>
                    <Pagination currenPage={voucherData.currentPage} setCurrentPage={setCurrentPage} total={total} />

                </div>


            ):<div className="text-center mt-[60px]">No data</div>
 
            }

        </div>
    )
}