import { useContext, useEffect ,useState} from "react";
// import { StepperContext } from "../../contexts/StepperContext";
import bookingService from "../services/booking";
import { useTranslation } from "react-i18next";
import { showModal } from "../reducers/modal";
import Calendar from "react-calendar";
import ModalResponse from "../containers/Dashboard/Booking/ModalResponse";
import { useAppDispatch } from "../hooks/hook";

export default function Test() {
    const dispatch = useAppDispatch();
    const [t] = useTranslation();

    const [triggerer, setTriggerer] = useState<boolean>(false);
    const [overlay, setOverlay] = useState<boolean>(false);
    const [date, setDate] = useState(new Date);

    const [staff, setStaff] = useState({
        total: 0,
        list: []
    });
    const [times, setTimes] = useState([]);
    const [matrix, setMatrix] = useState({});
    const [bookedMat, setBookMat] = useState({});
    const [buffer, setBuffer] = useState({});


    const showModalResponse = (id:string) => {
        bookingService.getBookingById(id).then((data)=> {
            dispatch(showModal(<ModalResponse booking={data} />))

        })
    }

    // const handleChange = (e: any) => {
    //     const { name, value } = e.target;
    //     setUserData({ ...userData, [name]: value });
    // };


    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    const getMat = (time:string, id:number, data:any) => {
        return (data[time])?
        data[time][id]:false;
    }

    const setMat = (time:string, id:number, val:any, data:any) => {
        if (!data[time]) data[time] = {};
        data[time][id] = val;
    }

    const handleDateChange = async function (param:Date) {
        let tab = (matrix as any);
        let bookedTab = (bookedMat as any);
        tab = {};
        bookedTab = {};

        await Promise.all([
            fetch ("/api/blacklist/"+dateToLocalTimeISOString(param).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                tab = res.tab;
                // console.log(res);
            }).catch((res) => {
                tab = {}
            }),
            fetch ("/api/booking/date/"+dateToLocalTimeISOString(param).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                // let temp = (matrix as any);
                // temp = {};
                for (let i = 0; i < res.total; i++){
                    let str:string = res.list[i].timetable;
                    str = str.substring(0,str.length - 3);
                    setMat(str,res.list[i].staff.id,res.list[i],bookedTab);
                    // setMat(str,res.list[i].staff.id,true,tab);
                }
            })
        ])
        setDate(param);
        setBookMat(bookedTab);
        setMatrix(tab);
        setBuffer({});
        setTriggerer(!triggerer)
    }

    const updateHandler = async function() {
        let tab = (matrix as any);
        let bookedTab = (bookedMat as any);
        let staffArray = staff;
        tab = {};
        bookedTab = {};

        await Promise.all([
            fetch ("/api/staff").then((res) => {
                return res.json();
            }).then((res) => {
                staffArray = res;
                // staffList();
            }),
            fetch ("/api/blacklist/"+dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                tab = res.tab;
                // console.log(res);
            }).catch((res) => {
                tab = {}
            }),
            fetch ("/api/booking/date/"+dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                // let temp = (matrix as any);
                // temp = {};
                for (let i = 0; i < res.total; i++){
                    let str:string = res.list[i].timetable;
                    str = str.substring(0,str.length - 3);
                    setMat(str,res.list[i].staff.id,res.list[i],bookedTab);
                    // setMat(str,res.list[i].staff.id,true,tab);
                }
            })
        ])

        const pad = (digit:number) => {return (digit<10)? '0' + digit.toString(): digit.toString()};
        let tArray = times as any;
        let i = 0;
        for (let h = 9; h < 18; h++){ // hour
            for (let m = 0; m < 60; m+=30) {
                const timeStr = (pad(h) + ":" + pad(m)) ;
                tArray[i] = (timeStr);
                i++;
            }
        }

        setTimes(tArray);
        setStaff(staffArray);
        setBookMat(bookedTab);
        setMatrix(tab);
        setTriggerer(!triggerer);
    }



    useEffect(() => {
        updateHandler();
    },[]);

    const cellHandler = (event:any) => {
        const staffId = (event.currentTarget.id as number);
        const time = event.target.parentElement.id;

        // if (!bookedMat(time,staffId,matrix)) {
            let temp = (buffer as any);
            (getMat(time,staffId,buffer))? setMat(time,staffId,false,temp):setMat(time,staffId,true,buffer);
            setBuffer(temp);
            setTriggerer(!triggerer)
            setOverlay(false);
            // console.log(buffer);
        // };
    }

    const colHandler = (event:any) => {
        const staffId = (event.currentTarget.id as number);
        let temp = (buffer as any);
        for (let i = 0; i < times.length; i++){
            setMat(times[i],staffId,true,buffer); 
        }
        setBuffer(temp);
        setTriggerer(!triggerer)
    }


    const rowHandler = (event:any) => {
    }

    const applyHandler = () => {
        let merge = {...matrix,...buffer} as any;

        for (const prop in merge){
            // merge[prop] = (merge as any)[prop].concat((buffer as any)[prop])     
            merge[prop] = { ...(matrix as any)[prop], ...(buffer as any)[prop] }
        }

        (async function() {

            await fetch ("/api/blacklist/"+dateToLocalTimeISOString(date).split('T')[0],{
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(merge)
            })
            handleDateChange(date);
        } )();

    }

    const unBookingHandler = () => {
        // console.log("THE QUICK");
        let merge = {...matrix,...buffer} as any;

        const temp = buffer as any;
        let i = 0;
        for (const prop in temp){
            for (const sub in temp[prop]){
                // console.log(sub);
                // setMat(prop,sub,false,temp)
                temp[prop][sub] =  false;
            }
        }



        for (const prop in merge){
            // console.log(merge[prop]);
            // merge[prop] = (merge as any)[prop].concat((buffer as any)[prop])     
            merge[prop] = { ...(matrix as any)[prop], ...(temp as any)[prop] }
        }
        // console.log(merge);

        (async function() {

            await fetch ("/api/blacklist/"+dateToLocalTimeISOString(date).split('T')[0],{
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(merge)
            })
            handleDateChange(date);
        } )();
    }


    const items = () => {
        // console.log("DSFJDISOFJDSOIFJ");
        const rows:JSX.Element[] = [];
        const setData = (index:number,time:string) => {
            const col = () => {
                const rows:JSX.Element[] = [];
                const list = staff.list;
                for (let c = 0; c < staff.total;c++) {

                    const temp = (list[c] as any);
                    // console.log(validMat(time,temp.id));
                    const isBooked = getMat(time,temp.id,bookedMat);
                    rows.push (

                    <td id={temp.id} onClick={ (!isBooked)?cellHandler:(()=>{
                        // console.log(isBooked);
                        showModalResponse(isBooked.id + "")
                    })} className={ 
                        ((isBooked)?"bg-red-300 ": 
                        (getMat(time,temp.id,buffer)?"bg-[#E5A380] ":
                        (getMat(time,temp.id,matrix)?"bg-[#ECEBEB] ":""))
                        ) +


                    "p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center"}></td>
                    )
                }
                return rows;
            }

            rows[index] = (
                <tr id={time} className="bg-white">
                    <td onClick={rowHandler} key="" className="p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center">{time}</td>
                    {col()}
                </tr>
            ) 
        }

        for (let i = 0; i < times.length; i++){
            
            setData(i,times[i]);
        }
        return rows 
    };

    const staffList = () => {
        const rows:JSX.Element[] = [];
        const list = staff.list;
        for (let s = 0; s < staff.total; s++){
            rows.push(
                <th id={(staff.list[s] as any).id} onClick={colHandler} className="min-w-[100px] p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE] text-[#64483B] text-center">{(list[s] as any).name}</th>
            )
        }
        return rows;
    }
    const countSelected = () => {
        const temp = buffer as any;
        let i = 0;
        for (const prop in temp){
            for (const sub in temp[prop]){
                temp[prop][sub] && i++;
            }
        }
        return i;
    }

    return (
        <div className="my-16 text-center">
            {triggerer && <></>}

            <h2 className="text-center text-text-primary lssm:text-px20 md:text-[48px] font-bold mt-[74px] mb-[48px] uppercase">{t("dashboard.request.fullybooking")}</h2>

            <div className="rounded-lg shadow overflow-scroll max-h-[510px] max-w-[100`] mx-auto">
                <table className="w-full rounded-lg">
                    <thead className="sticky top-[-1px] bg-[#F5ECE5] border-2 border-x-gray-200 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)]">
                        <tr>
                            <th 
                                className="p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE]">
                                <div className=" text-[#7E614F] flex flex-col items-center sc>768:text-px16">
                                    {/* <h3 className="text-px20 leading-[25px] whitespace-nowrap">17TH07, 2023</h3> */}
                                    <h3 className="text-px20 leading-[25px] whitespace-nowrap">{date.toDateString()}</h3>
                                    <a href="#" onClick={(e) => {
                                        e.preventDefault();
                                        setOverlay(!overlay)
                                    }}>
                                        <img src="/images/homepages/calender.png" className="w-[30px] mt-2" alt="calender.png"/>
                                    </a>
                                    <div className="relative w-[100%]">
                                    {overlay && (
                                        <div className="w-[20rem] absolute top-1 left-0 items-center bg-white border-2 border-t-border-box">
                                            <Calendar 
                                            value={date} onChange={(e:any) => handleDateChange(e)}
                                            
                                            />
                                        </div>
                                    )}
                                    </div>
                                </div>
                            </th>
                            {staffList()}
                        </tr>

                    </thead>
                    <tbody className="divide-y divide-gray-100">
                        {items()}
                    </tbody>
                </table>
            </div>

            <div className="ml-[-32px] inline-flex justify-start mt-12 mb-14 sm>640:hidden">
                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-1 flex justify-center bg-red-300">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Booked</span>

                    </div>
                </div>

                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-1 flex justify-center bg-[#ECEBEB]">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Busy</span>

                    </div>
                </div>

                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-2 flex justify-center ">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Available</span>
                    </div>
                </div>

                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-2 flex justify-center bg-[#DBA685]">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Selected {countSelected()}</span>
                    </div>
                </div>
            </div>

            <div className='container flex sm>640:flex-col justify-end mb-8 sm>640:mx-auto px-10'>
                {/* back button */}
                <button
                onClick={() => setBuffer({})}
                className={`bg-white text-[#65493A] uppercase py-2.5 px-[37px] sm>640:mt-8 rounded-xl font-semibold cursor-pointer border-2 border-slate-900 hover:bg-slate-700 hover:text-white transition duration-200 ease-in-out"}`}>
                    Deselect All
                </button>

                <button 
                onClick={() => unBookingHandler()}
                className='bg-[#A68276] text-white uppercase py-2.5 sm>640:ml-0 sm>640:mt-3 ml-2 px-[37px] rounded-xl font-semibold cursor-pointer hover:bg-primary hover:text-white transition duration-200 ease-in-out'>
                    UnBooking 
                </button>

                <button 
                onClick={() => applyHandler()}
                className='bg-[#A68276] text-white uppercase py-2.5 sm>640:ml-0 sm>640:mt-3 ml-2 px-[37px] rounded-xl font-semibold cursor-pointer hover:bg-primary hover:text-white transition duration-200 ease-in-out'>
                    Apply
                </button>
            </div>
        </div>);
}