import _debounce from "lodash/debounce";
import { Fragment, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useSearchParams } from "react-router-dom";
import Button from "../../components/Button";
import CartAmount from "../../components/cart/CartAmount";
import DialogConfirm from "../../components/DialogConfirm";
import Loadding from "../../components/Loadding/Loadding";
import InputSearch from "../../components/ManagerComponent/InputSearch";
import TitlePage from "../../components/ManagerComponent/TitlePage";
import Pagination from "../../components/Pagination";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { hideModal, showModal } from "../../reducers/modal";
import { pushPopup } from "../../reducers/popupSlice";
import {
  getAmount,
  searchAmount,
  setCurrenPage,
} from "../../reducers/amountmanagerSlice";
import amountServiece from "../../services/amount";
import { hostBE } from "../../types/host";

function AmountManager() {
  const [searchParams] = useSearchParams();
  const { t } = useTranslation();
  const navigator = useNavigate();
  const dispatch = useAppDispatch();
  const { amountList, error, loadding, total, currentPage } = useAppSelector(
    (state) => state.amountmanagerSlice
  );
  const { isEnglish } = useAppSelector((state) => state.translateSlice);
  const [valueSearch, setValueSearch] = useState<string>("");

  const handleChangeInputSearch = (inputData: string) => {
    navigator("");
    dispatch(setCurrenPage(1));
    setValueSearch(inputData);
  };
  const handleAddAmount = () => {
    navigator("add");
  };
  const handleEditAmount = async (id: number) => {
    navigator(`edit/${id}`);
  };
  const deleteAmount = async (id: number) => {
    try {
      const result = await amountServiece.deleteAmount(id);
      if (result) {
        dispatch(
          pushPopup({
            type: "SUCCESS",
            message: "Delete successfully.",
          })
        );
        if (currentPage === 1) {
          dispatch(getAmount({ page: currentPage, limit: 12 }));
        } else {
          dispatch(setCurrenPage(1));
        }
        navigator("");
      }
    } catch (error) {
      dispatch(
        pushPopup({
          type: "WARNING",
          message: isEnglish
            ? "Không xóa được amount!"
            : "Can't delete amount!",
        })
      );
    }
    dispatch(hideModal());
  };

  const handleDelete = (id: number) => {
    dispatch(
      showModal(
        <DialogConfirm
          message="Do you want to delete it?"
          onClick={() => {
            deleteAmount(id);
          }}
        />
      )
    );
  };

  // handle debounce iuputSearch
  const handleDebounceFn = (keySearch: string) => {
    if (keySearch != "") {
      dispatch(
        searchAmount({
          type: "",
          keySearch: keySearch,
          option: {
            page: currentPage - 1,
            limit: 9,
          },
        })
      );
    }
  };
  const debounceFn = useCallback(_debounce(handleDebounceFn, 1000), [
    valueSearch,
    currentPage,
  ]);

  useEffect(() => {
    if (searchParams.get("page")) {
      const numberPage = Number(searchParams.get("page"));
      setCurrenPage(numberPage);
    }
  }, [searchParams]);

  useEffect(() => {
    if (valueSearch != "") {
      if (currentPage == 1) {
        debounceFn(valueSearch);
      } else {
        dispatch(
          searchAmount({
            type: "",
            keySearch: valueSearch,
            option: {
              page: currentPage - 1,
              limit: 9,
            },
          })
        );
      }
    } else {
      dispatch(getAmount({ page: currentPage, limit: 12 }));
    }

    return () => {
      debounceFn.cancel();
    };
  }, [currentPage, valueSearch]);

  return (
    <div className="w-full">
      <TitlePage content="titleManager.amountTitle" />
      <div className="w-full flex sm:flex-row sm:flex-wrap flex-col justify-between items-center px-[10px]">
        <div className="2xl:w-3/4 m992:w-2/3 sm:w-[48%] w-full sm:mb-0 mb-4">
          <InputSearch
            ChangeInputFc={(param) => {
              handleChangeInputSearch(param);
            }}
          />
        </div>
        <div className="2xl:w-[24%] m992:w-[31%] sm:w-[48%] w-full">
          <Button
            color="primary"
            onClick={() => {
              handleAddAmount();
            }}
            className="w-full w-1920:py-[18px] xl:py-[17px] py-[12px] px-4 w-1920:px-10 2xl:px-0 flex "
            disabled={false}
            type="button"
          >
            <img className="mr-3" src={`${hostBE}/fe/add1.png`} alt="add" />
            <p className="sm-480:text-base text-sm font-medium text-white">
              {t("button.button_addAmount")}
            </p>
          </Button>
        </div>
      </div>
      {error ? (
        <div className="w-full sm:py-[60px] py-10 h-[200px] flex items-center justify-center">
          <p className="w-full text-text-red text-lg text-center">{error}</p>
        </div>
      ) : loadding ? (
        <div className="w-full sm:py-[60px] py-10 h-screen flex items-center justify-center">
          <Loadding />
        </div>
      ) : (
        <Fragment>
          <div className="w-full h-auto sm:py-[60px] py-10 flex flex-wrap">
            {amountList.length > 0 ? (
              amountList.map((amount, index) => {
                return (
                  <CartAmount
                    key={index}
                    cartContent={amount}
                    outstanding={true}
                    isGrid={true}
                    onClick={() => handleEditAmount(amount.id)}
                    onClickDelete={() => handleDelete(amount.id)}
                  />
                );
              })
            ) : (
              <div className="w-full min-h-[200px]">
                <p className="text-primary text-xl text-center">
                  Không có sản phẩm nào.
                </p>
              </div>
            )}
          </div>
          <div className="w-full  flex items-center justify-end sm>640:mb-0 mb-24">
            {total > 0 && (
              <Pagination
                total={Math.ceil(total / 12)}
                currenPage={currentPage}
                setCurrentPage={setCurrenPage}
              />
            )}
          </div>
        </Fragment>
      )}
    </div>
  );
}

export default AmountManager;
