import { useContext, useEffect ,useState} from "react";
import { StepperContext } from "../../contexts/StepperContext";
import { useTranslation } from "react-i18next";
import Calendar from "react-calendar";
import StaffManager from "../../services/staff"


export default function TimeStaff({data,setter}:any) {
    const [t] = useTranslation();

    const { userData, setUserData }: any = useContext(StepperContext);
    const [overlay, setOverlay] = useState<boolean>(false);
    const [date, setDate] = useState(data.bookingDate);
    const [staff, setStaff] = useState({
        total: 0,
        list: []
    });
    const [triggerer, setTriggerer] = useState<boolean>(false);
    const [weekend, setWeekend] = useState<boolean>(false);

    const [times, setTimes] = useState([
        "07:00",
        "08:00",
        "09:00",
        "11:00",
        "12:00",
        "13:00",
        "14:00",
        "15:00",
        "16:00",
        "17:00",
        "18:00",
        "19:00",
        "20:00",
        "21:00",
        "22:00",
        "23:00",
    ]);
    const [matrix, setMatrix] = useState({});
    const [onHighlightedId, setOnHighlightedId] = useState(data.staff.id);
    const [onHighlightedTime, setOnHighlightedTime] = useState(data.time);


    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setUserData({ ...userData, [name]: value });
    };

    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    // const bookedMat = (time:string, id:number) => {
    //     const tmp = (matrix as any);
    //     return (tmp[time])?
    //     ((tmp[time][id])?true:false):false;
    // }

    const bookedMat = (time:string, id:number, data:any) => {
        return (data[time])?
        data[time][id]:false;
    }

    const setMat = (time:string, id:number, val:boolean, data:any) => {
        if (!data[time]) data[time] = {};
        data[time][id] = val;
    }

    const updateHandler = async function() {
        let tab = (matrix as any);
        let staffArray = staff;
        tab = {};
        
        await Promise.all([
            StaffManager.get().then((res) => {
                console.log(res);
                staffArray = (res as any);
            }),
            StaffManager.blacklist( dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
                console.log("Success");
                tab = res;
            }).catch((res) => {
                tab = {}
            }),
            // fetch ("/api/blacklist/"+dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
            //     return res.json();
            // }).then((res) => {

            fetch ("/api/booking/date/"+dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                console.log(res);
                // let temp = (matrix as any);
                // temp = {};
                for (let i = 0; i < res.total; i++){
                    let str:string = res.list[i].timetable;
                    str = str.substring(0,str.length - 3);
                    setMat(str,res.list[i].staff.id,true,tab);
                    // tab[str] = {};
                    // tab[str][res.list[i].staff.id] = true;
                }
            })
        ])

        // const pad = (digit:number) => {return (digit<10)? '0' + digit.toString(): digit.toString()};
        // let tArray = times as any;
        // let i = 0;
        // for (let h = 9; h < 18; h++){ // hour
        //     for (let m = 0; m < 60; m+=30) {
        //         const timeStr = (pad(h) + ":" + pad(m)) ;
        //         tArray[i] = (timeStr);
        //         i++;
        //     }
        // }

        setStaff(staffArray);
        setMatrix(tab);
        setTriggerer(!triggerer);
    }

    const handleDateChange = async function(param:any) {
        setDate(param);
        const isWeekend = param.getDay()%6 ===0;
        setWeekend(isWeekend);
        if (isWeekend) 
            return;

        let currentDate = new Date;
        currentDate.setDate(currentDate.getDate() - 1)
        if (param.getTime() < currentDate.getTime()) {
            return;
        }

        // setDate(param);

        let tab = (matrix as any);
        await Promise.all([
            StaffManager.blacklist( dateToLocalTimeISOString(date).split('T')[0]).then((res) => {
                tab = res;
                console.log(tab);
            }).catch((res) => {
                tab = {}
            }),
            fetch ("/api/booking/date/"+dateToLocalTimeISOString(param).split('T')[0]).then((res) => {
                return res.json();
            }).then((res) => {
                // let temp = (matrix as any);
                // temp = {};
                for (let i = 0; i < res.total; i++){
                    let str:string = res.list[i].timetable;
                    str = str.substring(0,str.length - 3);
                    setMat(str,res.list[i].staff.id,true,tab);
                    // tab[str] = {};
                    // tab[str][res.list[i].staff.id] = true;
                }
            })
        ])
        setMatrix(tab);
        setTriggerer(!triggerer)
    }

    useEffect(() => {
        updateHandler();

    },[]);

    const celHandler = (event:any) => {
        const staffId = event.currentTarget.id;
        const time = event.target.parentElement.id;
        if (!weekend && !bookedMat(time,staffId,matrix) && !compareDate(time)) {
            data.staff.id = staffId;
            data.time = time;
            data.bookingDate = date;
            setter(data);
            setOnHighlightedId(staffId);
            setOnHighlightedTime(time);
            setOverlay(false);
        };
    };

    const getMin = (date: string) => {
        if (!date) return 0;
        const keys = date.split(":");
        if (keys.length < 2) return 0;
        return Number(keys[0])* 60 + Number(keys[1]);
    }

    const compareDate = (date: string) => {
        const temp = Object.keys(matrix).map((i) => {
            return i;
        });
        const result = temp.find((i) => {
            return getMin(i) <= getMin(date) + 60 && getMin(i) + 120 >= getMin(date);
        });
        return result;
    }

    // const items = () => {
    //     const pad = (digit:number) => {return (digit<10)? '0' + digit.toString(): digit.toString()};
    //     const rows:JSX.Element[] = [];

    //     const setMin = (h:number) => {
    //         const col = (time:string) => {
    //             const rows:JSX.Element[] = [];
    //             const list = staff.list;
    //             for (let c = 0; c < staff.total;c++) {
    //                 const temp = (list[c] as any);
    //                 rows.push(
    //                 <td id={temp.id} onClick={colHandler} className={ (( temp.id == onHighlightedId && 
    //                     time == onHighlightedTime && 
    //                     data.bookingDate.getTime() == date.getTime())?
    //                     "bg-[#E5A380] ":"") + 

    //                     (bookedMat(time,temp.id)?" bg-[#ECEBEB] ":"") +
    //                 "p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center"}></td>
    //                 )
    //             }
    //             return rows;
    //         }

    //         for (let m = 0; m < 60; m+=30) {
    //             const time = pad(h) + ":" + pad(m);
    //             rows.push(
    //                 <tr id={time} className="bg-white">
    //                     <td key="" className="p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center">{time}</td>
    //                     {col(time)}
    //                 </tr>
    //             ) 
    //         }
    //     }
    //     for (let h = 6; h < 24; h++){ // hour
    //         setMin(h);
    //     }
    //     return rows 
    // };

    const items = () => {
        const rows:JSX.Element[] = [];
        const setData = (index:number,time:string) => {
            const col = () => {
                const rows:JSX.Element[] = [];
                const list = staff.list;
                for (let c = 0; c < staff.total;c++) {

                    const temp = (list[c] as any);
                    // console.log(validMat(time,temp.id));
                    rows.push (

                    <td id={temp.id} onClick={celHandler} className={ 
                        (temp.id == onHighlightedId && time == onHighlightedTime && data.bookingDate.getTime() == date.getTime())? 
                        "bg-[#E5A380] ":"" +
                        // (bookedMat(time,temp.id,buffer)?"bg-[#E5A380] ":"") +
                        ( weekend || bookedMat(time,temp.id,matrix) || compareDate(time) ?"bg-[#ECEBEB] ":
                        "") +
                    "p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center"}></td>
                    )
                }
                return rows;
            }

            rows[index] = (
                <tr id={time} className="bg-white">
                    <td key="" className="p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center">{time}</td>
                    {col()}
                </tr>
            ) 
        }

        console.log("Start implement table");
        for (let i = 0; i < times.length; i++) {
            setData(i,times[i]);
        }
        return rows 
    };

    const staffList = () => {
        const rows:JSX.Element[] = [];
        const list = staff.list;
        for (let s = 0; s < staff.total; s++){
            rows.push(
                <th className="min-w-[100px] p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE] text-[#64483B] text-center">{(list[s] as any).name}</th>
            )
        }
        return rows;
    }

    return (
        <div className="">
            <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
            <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description")}</p>

            <div className="rounded-lg shadow overflow-scroll max-h-[405px] max-w-[801px]">
                <table className="w-full rounded-lg">
                    <thead className="sticky top-[-1px] bg-[#F5ECE5] border-2 border-x-gray-200 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)]">
                        <tr>
                            <th 
                                className="p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE]">
                                <div className=" text-[#7E614F] flex flex-col items-center sc>768:text-px16">
                                    {/* <h3 className="text-px20 leading-[25px] whitespace-nowrap">17TH07, 2023</h3> */}
                                    <h3 className="text-px20 leading-[25px] whitespace-nowrap">{date.toDateString()}</h3>
                                    <a href="#" onClick={(e) => {
                                        e.preventDefault();
                                        setOverlay(!overlay)
                                    }}>
                                    
                                        <img src="/images/homepages/calender.png" className="w-[30px] mt-2" alt="calender.png"/>
                                    </a>
                                    <div className="relative w-[100%]">
                                    {overlay && (
                                        <div className="w-[20rem] absolute top-1 left-0 items-center bg-white border-2 border-t-border-box">
                                            <Calendar 
                                            value={date} onChange={(e:any) => handleDateChange(e)}
                                            
                                            />
                                        </div>
                                    )}
                                    </div>
                                </div>
                            </th>
                            {staffList()}
                        </tr>

                    </thead>
                    <tbody className="divide-y divide-gray-100">
                        {items()} 
                    </tbody>
                </table>
            </div>

            <div className="ml-[-32px] h-60 flex justify-start pt-6">
                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-1 flex justify-center bg-[#ECEBEB]">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Booked</span>

                    </div>
                </div>

                <div className="pl-10 h-[37px] flex">
                    <div className="w-[75px] border-2 flex justify-center ">
                    </div>
                    <div className="pl-2 text-center flex justify-center">
                        <span className="my-auto">Available</span>
                    </div>
                </div>
            </div>

        </div>);
}