import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import { useTranslation } from "react-i18next";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { useAppSelector } from "../../hooks/hook";
import { Autoplay, FreeMode, Navigation, Thumbs } from "swiper";
import { useState } from "react";

export default function SliderBannerHome() {
    const banner = useAppSelector(state => state.bannerSlice);
    const [t] = useTranslation();
    const [activeThumb, setThumbActive] = useState<any>(null);
    return (
        <div className="w-full">
            <div className="relative w-full min-h-full h-full max-w-full max-h-full max-xl:hidden !overflow-visible">
                <div className="absolute top-[10%] -left-[10%] bg-white px-6 pt-12 pb-5 text-center rounded-t-[300px] z-50">
                    <span className="Valky text-[#2F2D38] text-[38px] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[16px] 2xl:text-[16px]">{t("home.banner.sale")}</span>
                    <div className="text-[#2F2D38] Valky lssm:w-fit xl:w-max pt-6 mt-6 border-solid border-t-2 border-[#2F2D38] sm-480:text-[30px] lssm:text-[24px] lg:text-[30px] font-bold uppercase xl:min-text-[30px] mx-auto relative z-[1] animate__animated animate__fadeInDown">
                        {t("home.banner.50")}
                    </div>
                </div>
                <img src="/images/homepages/iconbanner.png" className="absolute -top-[4.5%] left-[45%] z-50" alt="" />
                <Swiper
                    slidesPerView={1}
                    thumbs={{ swiper: activeThumb && !activeThumb.destroyed ? activeThumb : null }}

                    spaceBetween={0}
                    initialSlide={1}

                    // slidesPerGroup={1}
                    loop={false}
                    loopFillGroupWithBlank={true}
                    autoplay={{
                        delay: 5000,
                        disableOnInteraction: false,
                    }}

                    modules={[Navigation, Autoplay, Thumbs, FreeMode]}
                    className="min-w-full min-h-full !w-[621px] aspect-[621/732] rounded-t-[300px] bg-[#FCF5EF] btn-banner after:top-0 after:left-0 lg:after:top-0 lg:after:left-0"
                >

                    {
                        banner.banners.map((item, index) => {
                            return (

                                <SwiperSlide key={item.id} className="min-w-full min-h-full">
                                    <img className="banner_home_primary min-w-full min-h-full wfull rounded-t-[300px] object-cover p-6 z-50 aspect-[621/732]" src={item?.imageUrl ?? ""} alt={""} />
                                </SwiperSlide>

                            )
                        })
                    }


                </Swiper>
                <div className="absolute max-w-fit  bottom-1 left-[50%] translate-x-[-50%] z-[3]">

                    <Swiper
                        slidesPerView={banner.banners.length}
                        // spaceBetween={30}
                        // pagination={{
                        //   clickable: true,
                        // }}
                        // navigation={true}
                        initialSlide={1}
                        freeMode={true}
                        onSwiper={setThumbActive}
                        watchSlidesProgress={true}
                        modules={[Navigation, Thumbs, FreeMode]}
                        className="h-[20px]   swiper-banner-home"
                    >
                        {
                            banner.banners.map((item: any, index) => {
                                return <SwiperSlide key={index} className="w-full h-[3px]">
                                    <div className="w-[12px] rounded-[50%] h-[12px] mr-[12px] bg-[#ccc] cursor-pointer">

                                    </div>
                                </SwiperSlide>
                            })
                        }
                    </Swiper>
                </div>

            </div>
            <div className="relative w-full min-h-full h-full max-w-full max-h-full xl:hidden">
                <Swiper
                    slidesPerView={1}
                    thumbs={{ swiper: activeThumb && !activeThumb.destroyed ? activeThumb : null }}

                    spaceBetween={0}
                    initialSlide={1}

                    // slidesPerGroup={1}
                    loop={false}
                    loopFillGroupWithBlank={true}
                    autoplay={{
                        delay: 5000,
                        disableOnInteraction: false,
                    }}

                    modules={[Navigation, Autoplay, Thumbs, FreeMode]}
                    className="min-w-full min-h-full"
                >

                    {
                        banner.banners.map((item, index) => {
                            return (

                                <SwiperSlide key={item.id} className="min-w-full min-h-full  bg-slate-500">
                                    <img className="banner_home_primarys w-full  object-cover" src={item?.imageUrl ?? ""} alt={""} />

                                </SwiperSlide>

                            )
                        })
                    }


                </Swiper>
                <div className="absolute max-w-fit  bottom-1 left-[50%] translate-x-[-50%] z-[3]">

                    <Swiper
                        slidesPerView={banner.banners.length}
                        // spaceBetween={30}
                        // pagination={{
                        //   clickable: true,
                        // }}
                        // navigation={true}
                        initialSlide={1}
                        freeMode={true}
                        onSwiper={setThumbActive}
                        watchSlidesProgress={true}
                        modules={[Navigation, Thumbs, FreeMode]}
                        className="h-[20px]   swiper-banner-home"
                    >
                        {
                            banner.banners.map((item: any, index) => {
                                return <SwiperSlide key={index} className="w-full h-[3px]">
                                    <div className="w-[12px] rounded-[50%] h-[12px] mr-[12px] bg-[#ccc] cursor-pointer">

                                    </div>
                                </SwiperSlide>
                            })
                        }
                    </Swiper>
                </div>

            </div>
        </div>
    )
}