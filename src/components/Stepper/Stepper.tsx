import { update } from 'lodash';
import React, { useEffect, useState, useRef } from 'react';

const Stepper = ({steps, currentStep}:any) => {

    const [newStep, setNewStep] = useState<any>([]);
    const stepRef = useRef();

    const updateStep = (stepNumber: any, steps: any) => {
        const newSteps = [...steps]
        let count = 0;

        while (count < newSteps.length) {
            // current steps
            if (count == stepNumber) {
                newSteps[count] = {
                    ...newSteps[count],
                    highlighted:true,
                    selected: true,
                    completed: true,
                };
                count++;
            }
            // step completed
            else if(count < stepNumber) {
                newSteps[count] = {
                    ...newSteps[count],
                    highlighted: false,
                    selected: true,
                    completed: true,
                };
                count++;
            }
            // step pending
            else {
                newSteps[count] = {
                    ...newSteps[count],
                    highlighted: false,
                    selected: false,
                    completed: false,
                };
                count++;
            }
        }
        return newSteps;
    };

    useEffect(() => {
        //create Object
        const stepsState = steps.map((step: any, index: any) =>
            Object.assign(
                {},
                {
                    description: step,
                    completed: false,
                    highlighted: index === 0 ? true : false,
                    selected: index === 0 ? true : false,
                }
            )
        );

        stepRef.current = stepsState;
        const current = updateStep(currentStep - 1, stepRef.current);
        setNewStep(current);

    }, [steps, currentStep])
    

    const displaySteps = newStep.map((step:any, index:any) => {
        return (
            <div key = {index} className={index != newStep.length -1 ?  'w-full flex items-center' :  'flex items-center'}>
                <div className='relative flex flex-col items-center text-teal-600'>
                    <div className={`rounded-full transition duration-500 ease-in-out border-[7px] border-grey-300 ${step.completed ? "h-5 w-5" : "hidden"} flex items center justify-center p-auto ${step.selected ? "bg-[#F1B290] text-white font-bold border border-[#F1B290]" : ""}`}>
                        {/* Display number */}
                        {step.completed ? (
                            // <span className='text-white font-bold text-xl my-auto'>.</span>
                            <img src="/images/homepages/dot.png" className="w-[7px] h-[7px]" alt="icon-topic1.png"/>
                        ):(
                            <p className='hidden'>{index + 1}</p>
                        )}
                    </div>
                    <div className={`absolute ${step.completed ? "-top-6" : "-top-9"} sc>768:hidden text-center w-32 text-xs font-medium uppercase ${step.highlighted ? "text-gray-900" : "text-gray-400"}`}>
                        {/* Display Description */}
                        {step.description}
                    </div>
                </div>
                <div className={`flex-auto border-t-4 transition duration-500 ease-in-out ${step.completed ? "border-[#F1B290]" : "border-gray-300"}`}>
                    {/* Display line */}
                </div>
            </div>
        );
    });

    return (
        <div className='mx-4 p-4 flex justify-between items-center'>
            {displaySteps}
        </div>);
};

export default Stepper;