type Props = {
  width: number | string;
  height: number | string;
};

export default function Map({ width, height }: Props) {
  return (
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3186.434014603149!2d174.90210251222248!3d-36.99943878782422!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d4d73eb60247f%3A0xa331b80c2933c6fc!2s81%20Eugenia%20Rise%2C%20Totara%20Heights%2C%20Auckland%202105%2C%20New%20Zealand!5e0!3m2!1svi!2s!4v1693558799394!5m2!1svi!2s"
      width={width}
      height={height}
    ></iframe>
  );
}
