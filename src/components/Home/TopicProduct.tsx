import clsx from "clsx";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { CgArrowLongRight } from "react-icons/cg";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import useInView from "../../hooks/useInView";
import { changeCategoryChecked } from "../../reducers/categorySlice";
import { getProductActiveHome } from "../../reducers/productPublic";
import { hostBE } from "../../types/host";

import Button from "../Button";
import useViewport from "../../hooks/useViewPort";


const datas = [
  {
    title: "home.topic.brows",
    titles: "home.topic.brows",
    menuImage: "https://kbeautyartistry.co.nz/fe/Brows.png",
    des: "home.description_brows",
    listImage: [
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-1.png",
      //   grid: 6
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-12.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-13.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-11.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-10.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-6.png",
      //   grid: 6
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-7.png",
      //   grid: 4
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-8.png",
      //   grid: 4
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/Brows-9.png",
      //   grid: 4
      // },
    ]
  },
  {
    title: "home.topic.lips",
    titles: "home.topic.lips",
    menuImage: "https://kbeautyartistry.co.nz/fe/lips.png",
    des: "home.description_lip",
    listImage: [
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lips-14.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lips-12.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lips-13.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lips-15.png",
      //   grid: 3
      // }
    ]
  },
  // {
  //   title: "home.topic.nails",
  //   menuImage: "https://kbeautyartistry.co.nz/fe/nail.png",
  //   des: "home.description_nails",
  //   listImage: [
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-1.png",
  //     //   grid: 6
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-2.png",
  //     //   grid: 3
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-3.png",
  //     //   grid: 3
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-4.png",
  //     //   grid: 3
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-5.png",
  //     //   grid: 3
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-6.png",
  //     //   grid: 6
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-7.png",
  //     //   grid: 4
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-8.png",
  //     //   grid: 4
  //     // },
  //     // {
  //     //   image: "https://kbeautyartistry.co.nz/fe/nail-9.png",
  //     //   grid: 4
  //     // }
  //   ]
  // },
  {
    title: "home.topic.lashes",
    titles: "home.topic.lashes",
    menuImage: "https://kbeautyartistry.co.nz/fe/lashes.png",
    des: "home.description_lashes",
    listImage: [
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-10.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-11.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-4.png",
      //   grid: 3
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-6.png",
      //   grid: 3
      // }
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-6.png",
      //   grid: 6
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-7.png",
      //   grid: 4
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-8.png",
      //   grid: 4
      // },
      // {
      //   image: "https://kbeautyartistry.co.nz/fe/lashes-9.png",
      //   grid: 4
      // }
    ]
  },
  {
    title: "home.topic.massage",
    titles: "home.topic.other",
    menuImage: "https://kbeautyartistry.co.nz/fe/massage.png",
    des: "home.description_massage",
    listImage: [
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-1.png",
        grid: 6
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-2.png",
        grid: 3
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-3.png",
        grid: 3
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-4.png",
        grid: 3
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-5.png",
        grid: 3
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-6.png",
        grid: 6
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-7.png",
        grid: 4
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-8.png",
        grid: 4
      },
      {
        image: "https://kbeautyartistry.co.nz/fe/massage-9.png",
        grid: 4
      }
    ]
  }
]



export default function TopicProduct() {
  
  const {width} = useViewport();
  const { ref, isInView } = useInView();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const translate = useAppSelector((state) => state.translateSlice);
  const [indexActive, setIndexActive] = useState<number>(0)
  const categories = useAppSelector(
    (state) => state.productPublic.productActiveHome
  );
  const [t] = useTranslation();

  const handleNavigate = (id: number) => {
    navigate("/sanpham");
    dispatch(changeCategoryChecked(id));
  };
  useEffect(() => {
    // if (productActiveHome.length === 0) {
    dispatch(getProductActiveHome());
    // }
  }, []);

  return (
    <div>
      <div className="text-center">
        <h2 className="Valky 2xl:mb-8 lssm:mb-0 text-[#F5E3D5] text-[48px] font-medium 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[38px] 2xl:text-[56px]">{t("home.topic.ks")}</h2>
        <span className="Valky text-[#2F2D38] text-[48px] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]">{t("home.topic.beautyful")}</span>
      </div>
      <div className="grid grid-cols-12 2xl:my-16 xl:my-12 lg:my-8 max-lgs:my-8">
        {datas.map((data, index) => {
          return <div onClick={() => setIndexActive(index)} key={data.title} className="relative aspect-[362/160] w-[90%] lg:col-span-3 max-lgs:col-span-5 rounded-[24px] mx-auto max-lgs:mb-[5%] cursor-pointer"
          style={{
            backgroundImage: `url('${data.menuImage}')`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}>
            <div className="absolute bottom-0 right-0 w-full">
                <div className="w-full opacity-30 bg-[#000000] aspect-[362/160] rounded-[24px]"></div>
            </div>
            <h3 className="text-white h-full flex justify-center items-center iframeVideo z-50 2xl:text-[30px] xl:text-[26px] lg:text-[20px] md:text-[28px] sm-480:text-[24px]">{t(data.title)}</h3>
          </div>
        })}
      </div>
      <div className="xl:my-24 m992:my-16 sm:my-14 lssm:my-12">
        <div
          className={clsx(
            "flex flex-col text-center text-text-title 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
            { "animate__animated animate__fadeInRight": isInView }
            )}
            >
          <span className="Valky mb-8 text-[#2F2D38] 2xl:text-[48px] xl:text-[42px] lg:text-[35px] sm-480:text-[24px] uppercase font-bold">{t(datas?.[indexActive]?.titles)}</span>
          <div>
            <p className={clsx(
                "my-5 lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-16 sm:mb-12 lssm:mb-10 text-[#2F2D38] px-[10%] text-center",
                { "animate__animated animate__flash": isInView }
              )}>{t(datas?.[indexActive]?.des)}</p>
          </div>
          <div className="grid grid-cols-12 gap-5">
          {datas?.[indexActive].listImage.map((image, index) => {
            return <div key={index} style={{ gridColumn: `span ${width >= 1280 ? image.grid : width > 640 ? 6 : 12} / span ${width > 1280 ? image.grid : width > 640 ? 6 : 12}` }} className="h-[363px] overflow-hidden rounded-[24px]">
                  <img src={`${image.image}`} alt="" className="w-full h-full object-cover rounded-[24px] duration-700 hover:scale-125" />
            </div>
          })}
          </div>
        </div>
      </div>
    </div>
  );
}
