import clsx from "clsx";
import { useTranslation } from "react-i18next";
import { BiEdit } from "react-icons/bi";
import { RiDeleteBin6Line } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { putPriorityPackage } from "../../reducers/packagemanagerSlice";
import { packageType } from "../../typeProps/Packagetype";
import { hostBE } from "../../types/host";
import Button from "../Button";

type Props = {
  cartContent: packageType;
  isGrid?: boolean;
  outstanding?: boolean;
  onClick?: () => void;
  onClickDelete?: () => void;
  propsClas?: string;
};

function CartPackage({
  cartContent,
  onClick,
  onClickDelete,
  outstanding,
  isGrid,
  propsClas,
}: Props) {
  const checkClass = isGrid;
  const { t, i18n } = useTranslation();

  const dispatch = useAppDispatch();
  const { isEnglish } = useAppSelector((state) => state.translateSlice);
  const navigate = useNavigate();
  const handleString = (titleString: string, lengthEnnable: number) => {
    if (titleString.length > lengthEnnable - 3) {
      const newTitle = titleString.slice(0, lengthEnnable - 3) + "...";
      return newTitle;
    } else {
      return titleString;
    }
  };
  const handleContact = () => {
    navigate("/lien-he");
  };

  const handlePutPriority = (id: number) => {
    dispatch(putPriorityPackage(id));
  };
  return (
    <div
      className={
        propsClas
          ? propsClas
          : clsx(
              "px-[5px] m992:px-[10px] ",
              outstanding !== undefined
                ? "xl:px-[10px] md:px-3"
                : "xl:px-[25px] ",
              checkClass
                ? outstanding !== undefined
                  ? "w-1920:w-1/2 xl:w-2/6 lg:w-2/4 m992:w-4/12 sm-480:w-2/4 w-full"
                  : "xl:w-1/3 md:w-1/3 sm-480:w-6/12 sm-390:w-6/12 w-full"
                : "w-full"
            )
      }
    >
      <div
        className={clsx(
          "w-full border rounded border-[#EFEFEF]  2xl:mb-12 sm-480:mb-8 mb-[10px]",
          !checkClass &&
            "sm-390:flex sm-390:items-center sm-390:justify-between"
        )}
      >
        <div
          className={clsx(
            "flex flex-col justify-between px-3 pb-6",
            checkClass
              ? "w-full ms-480:pt-3 pt-1 xl:px-7 "
              : "sm-390:w-4/5 w-full sm-480:py-0 sm-480:px-4 sm-390:py-1 md:px-12"
          )}
        >

          <div className="w-full mb-4">
            <p
              className={clsx(
                "sm-480:text-xl text-base mt-5 h-auto font-bold 2xl:mb-6 mb-3 text-bg_blue_bold line-clamp-1"
              )}
            >
              {cartContent.title}
            </p>
            <div className="flex justify-start">
              <p
                className={clsx(
                  "sm-480:text-base text-sm font-medium font-bold text-bg_blue_bold 2xl:mb-5 sm:mb-4  mb-3 mr-4",
                  !checkClass && "sm-390:mb-1"
                )}
              >
                {t("cart.descriptionproject")}
              </p>
              <p
                className={clsx(
                  "sm-480:text-base text-sm h-auto text-text-gray font-normal mb-4 line-clamp-3"
                )}
              >
              {cartContent.price}
              </p>
            </div>
            <div className="flex justify-start">
              <p
                className={clsx(
                  "sm-480:text-base text-sm font-medium font-bold text-bg_blue_bold 2xl:mb-5 sm:mb-4 mb-3 mr-4",
                  !checkClass && "sm-390:mb-1"
                )}
              >
                {t("cart.descriptiondes")}
              </p>
              <p
                className={clsx(
                  "sm-480:text-base text-sm h-auto text-text-gray font-normal mb-4 line-clamp-3"
                )}
              >
                {cartContent.description}
              </p>
            </div>
          </div>

          <div className="w-full">
            <Button
              color="primary"
              onClick={onClick}
              className={clsx(
                "w-full  w-1920:px-4 2xl:py-3 sm-390:py-3  xl:text-sm sm:px-0 sm-390:px-6 px-2 py-[6px] text-xs mb-[10px] flex"
              )}
              disabled={false}
              type="button"
            >
              <BiEdit className="text-2xl sm-480:w-6 sm-480:h-6 md:mr-5 sm-390:mr-5 mr-2 sm-390:w-[22px] sm-390:h-[22px]" />
              <p className="2xl:text-base xl:text-sm sm-480:text-base text-sm font-normal sm-390:text-base text-white">
                {t("cart.editPackage")}
              </p>
            </Button>
            <Button
              color="empty"
              onClick={onClickDelete}
              className={clsx(
                "w-full  w-1920:px-4 2xl:py-3 sm-390:py-3 sm-390:px-6 px-2 py-[6px] text-xs flex border-text-red "
              )}
              disabled={false}
              type="button"
            >
              <RiDeleteBin6Line className="text-xl sm-480:w-6 sm-480:h-6 md:mr-5 sm-390:mr-5 mr-2 sm-390:w-[22px] sm-390:h-[22px] text-text-red" />
              <p className="font-normal sm-480:text-base text-sm text-text-red">
                {t("cart.deletepackage")}
              </p>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CartPackage;
