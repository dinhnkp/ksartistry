import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { RootState } from '../configs/redux/store';
import { hideModal } from '../reducers/modal';
import { pushPopup } from '../reducers/popupSlice';
import { editLocationSuccess, editLocation, getLocation, getLocationFail, getLocationSuccess, searchLocation, searchLocationSuccess } from '../reducers/locationmanagerSlice';
import { TranslateState } from '../reducers/translate';
import locationServiece from '../services/location';
import { locationResult, locationType, searchLocationParam } from '../typeProps/Locationtype';
import { Option } from './../types';


function* getLocationSaga({payload}: PayloadAction<Option>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: locationResult = yield call(locationServiece.get,payload);
        yield put(getLocationSuccess(res))
    } catch (error) {
        yield put(getLocationFail("Unable to display locations list"))
    }
}

function* searchLocationSaga({payload}: PayloadAction<searchLocationParam>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res : locationResult = yield call(locationServiece.searchLocation, payload);
        yield put(searchLocationSuccess(res))
        
    } catch (error) {
        yield put(getLocationFail("No locations found"))
    }
}

function* editLocationSaga({payload}: PayloadAction<locationType>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: locationType = yield call(locationServiece.editLocation,payload);
        yield put(editLocationSuccess(res))
        yield put( pushPopup({
            type: "SUCCESS",
            message: "Edit successfully.",
          }))

          yield put(hideModal());
    } catch (error) {
        yield put(
            pushPopup({
                type: "WARNING",
                message: "Edit unsuccessfully",
              })
        )
    }
}



export default function* projectManagerSagaMid(){
    yield takeLatest(getLocation.type,getLocationSaga);
    yield takeLatest(searchLocation.type,searchLocationSaga);
    yield takeLatest(editLocation.type,editLocationSaga);
}