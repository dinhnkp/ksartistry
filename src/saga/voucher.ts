import { Voucher, FormContact, Option, ResponsiveData } from './../types';
import { PayloadAction } from '@reduxjs/toolkit';
import { call, debounce, put, select, takeLatest } from 'redux-saga/effects';
import { deleteVoucherFail, deleteVoucherReduce, deleteVoucherSuccess, getVoucher, getVoucherFail, getVoucherSuccess, searchVoucher, searchVoucherFail, searchVoucherSuccess, setCurrentPage } from '../reducers/vouchermanagerSlice';
import voucherService from '../services/voucher';
import { hideModal } from '../reducers/modal';
import { pushPopup } from '../reducers/popupSlice';
import { RootState } from '../configs/redux/store';
import { TranslateState } from '../reducers/translate';


function* getVoucherSaga({payload}:PayloadAction<Option>) {
    try {
        const res:ResponsiveData<Voucher> = yield call(voucherService.getVouchers,payload);
        yield put(getVoucherSuccess(res));
    }catch(error) {
        yield put(getVoucherFail());
    }   


}

function* deleteVoucherSaga ({payload}:PayloadAction<number>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);  

        try {
            const res:boolean = yield call(voucherService.deleteVoucher, payload);
            if(res) {
                const {voucherList, currentPage} = yield select((state:RootState) => state.voucherSlice)
                if(voucherList.length === 1 && currentPage > 1) {
                   yield put(setCurrentPage(currentPage - 1))
                }else {
                    yield put(getVoucher({
                        page: currentPage,
                        limit: 9
                    }))
                }
                yield put(deleteVoucherSuccess(payload))
                yield put(hideModal());
                yield put(pushPopup({
                    type: "SUCCESS",
                    message: "Delete successfully"
                }));
            }
        }catch(error) {
            yield put(deleteVoucherFail());
            yield put(hideModal());
            yield put(pushPopup({
                type: "ERROR",
                message: "Delete failed"
            }));
        }
}


function* searchVoucherSaga({payload}:PayloadAction<{keyword:string, option:Option}>) {
    try {
        if(payload.keyword.trim() !== "") {
            const res:ResponsiveData<Voucher> = yield call(voucherService.search, payload);
            yield put(searchVoucherSuccess(res));
        }else {
            yield put(getVoucher(payload.option))
        }
    }catch(error) {
        yield put(searchVoucherFail());

    }
}


export default function* contactSaga() {
    yield takeLatest(getVoucher.type, getVoucherSaga)
    yield takeLatest(deleteVoucherReduce.type, deleteVoucherSaga)
    yield debounce(1000, searchVoucher.type, searchVoucherSaga)
}