import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { RootState } from '../configs/redux/store';
import { hideModal } from '../reducers/modal';
import { pushPopup } from '../reducers/popupSlice';
import { editAmountSuccess, editAmount, getAmount, getAmountFail, getAmountSuccess, searchAmount, searchAmountSuccess } from '../reducers/amountmanagerSlice';
import { TranslateState } from '../reducers/translate';
import amountServiece from '../services/amount';
import { amountResult, amountType, searchAmountParam } from '../typeProps/Amounttype';
import { Option } from './../types';


function* getAmountSaga({payload}: PayloadAction<Option>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: amountResult = yield call(amountServiece.get,payload);
        yield put(getAmountSuccess(res))
    } catch (error) {
        yield put(getAmountFail("Unable to display amount list"))
    }
}

function* searchAmountSaga({payload}: PayloadAction<searchAmountParam>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res : amountResult = yield call(amountServiece.searchAmount, payload);
        yield put(searchAmountSuccess(res))
        
    } catch (error) {
        yield put(getAmountFail("No amount found"))
    }
}

function* editAmountSaga({payload}: PayloadAction<amountType>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: amountType = yield call(amountServiece.editAmount,payload);
        yield put(editAmountSuccess(res))
        yield put( pushPopup({
            type: "SUCCESS",
            message: "Edit successfully.",
          }))

          yield put(hideModal());
    } catch (error) {
        yield put(
            pushPopup({
                type: "WARNING",
                message: "Edit unsuccessfully.",
              })
        )
    }
}



export default function* projectManagerSagaMid(){
    yield takeLatest(getAmount.type,getAmountSaga);
    yield takeLatest(searchAmount.type,searchAmountSaga);
    yield takeLatest(editAmount.type,editAmountSaga);
}