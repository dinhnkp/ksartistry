import { Booking, FormContact, Option, ResponsiveData } from './../types';
import { PayloadAction } from '@reduxjs/toolkit';
import { call, debounce, put, select, takeLatest } from 'redux-saga/effects';
import { deleteBookingFail, deleteBookingReduce, deleteBookingSuccess, getBooking, getBookingFail, getBookingSuccess, searchBooking, searchBookingFail, searchBookingSuccess, setCurrentPage } from '../reducers/bookingmanagerSlice';
import bookingService from '../services/booking';
import { hideModal } from '../reducers/modal';
import { pushPopup } from '../reducers/popupSlice';
import { RootState } from '../configs/redux/store';
import { TranslateState } from '../reducers/translate';


function* getBookingSaga({payload}:PayloadAction<Option>) {
    try {
        const res:ResponsiveData<Booking> = yield call(bookingService.getBooking,payload);
        yield put(getBookingSuccess(res));
    }catch(error) {
        yield put(getBookingFail());
    }   


}

function* deleteBookingSaga ({payload}:PayloadAction<number>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);  

        try {
            const res:boolean = yield call(bookingService.deleteBooking, payload);
            if(res) {
                const {bookingList, currentPage} = yield select((state:RootState) => state.bookingSlice)
                if(bookingList.length === 1 && currentPage > 1) {
                   yield put(setCurrentPage(currentPage - 1))
                }else {
                    yield put(getBooking({
                        page: currentPage,
                        limit: 9
                    }))
                }
                yield put(deleteBookingSuccess(payload))
                yield put(hideModal());
                yield put(pushPopup({
                    type: "SUCCESS",
                    message: "Delete successfully"
                }));
            }
        }catch(error) {
            yield put(deleteBookingFail());
            yield put(hideModal());
            yield put(pushPopup({
                type: "ERROR",
                message: "Delete failed"
            }));
        }
}


function* searchBookingSaga({payload}:PayloadAction<{keyword:string, option:Option}>) {
    try {
        if(payload.keyword.trim() !== "") {
            const res:ResponsiveData<Booking> = yield call(bookingService.search, payload);
            yield put(searchBookingSuccess(res));
        }else {
            yield put(getBooking(payload.option))
        }
    }catch(error) {
        yield put(searchBookingFail());

    }
}


export default function* bookingSaga() {
    yield takeLatest(getBooking.type, getBookingSaga)
    yield takeLatest(deleteBookingReduce.type, deleteBookingSaga)
    yield debounce(1000, searchBooking.type, searchBookingSaga)
}