import { ConsoleSqlOutlined } from "@ant-design/icons";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { packageResult, packageType, searchPackageParam } from "../typeProps/Packagetype";
import { Option, Package } from "../types";

interface packageInterface {
    loadding: boolean;
    error:null | string;
    total: number;
    currentPage: number;
    packageList: packageType[]
}

const initialState: packageInterface= {
    loadding:false,
    error:null,
    total: 0,
    currentPage: 1,
    packageList: []
}

export const  packageManagerSlice = createSlice({
    name: "packageManagerSlice",
    initialState,
    reducers: {
        setCurrenPage(state,action: PayloadAction<number>){
            state.currentPage = action.payload;
        },
        getPackage(state,action: PayloadAction<Option>){
            state.loadding= true;
        },
        getPackageSuccess (state,action:PayloadAction<packageResult>){
            state.total = action.payload.total;
            state.packageList = action.payload.list;
            state.loadding = false;
            
        },
        getPackageFail(state,action:PayloadAction<string>){
            state.error = action.payload;
        },
        searchPackage(state,action:PayloadAction<searchPackageParam>){
            state.loadding = true;
        },
        searchPackageSuccess(state,action:PayloadAction<packageResult>){
            state.total = action.payload.total;
            state.packageList = action.payload.list;
            state.loadding = false;
        },
        editPackage (state,action:PayloadAction<Package>){
           
        },
        editPackageSuccess(state,action:PayloadAction<packageType>){
            const newLocation = state.packageList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item;
            })
            state.packageList = newLocation
        },
        putPriorityPackage (state,action:PayloadAction<number>){
        },
        putPrioritySuccess(state,action:PayloadAction<packageType>){
            const newState = state.packageList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item
            })
            state.packageList = newState;
        }
    }
});


export const {setCurrenPage, getPackage,getPackageSuccess,getPackageFail,searchPackage,searchPackageSuccess,editPackage,editPackageSuccess,putPriorityPackage,putPrioritySuccess} = packageManagerSlice.actions;

export default packageManagerSlice.reducer;