import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import {Option, Staff, ResponsiveData } from '../types'

// import {ModalState} from "../types"

// Define a type for the slice state
interface StaffState {
    listStaff: Staff[],
    total: number,
    isLoading: boolean,
    totalCandidates:number,
    currentPage: number;

}

// Define the initial state using that type
const initialState: StaffState = {
    listStaff: [],
    total: 0,
    isLoading: false,
    totalCandidates: 0,
    currentPage: 1
}

export const staffSlice = createSlice({
  name: 'staff',
  initialState,
  reducers: {
    setCurrentPage: (state, action:PayloadAction<number>) => {
        state.currentPage = action.payload;
    },

    createStaffSuccess: (state, action:PayloadAction<Staff>) => {
        state.listStaff = [action.payload, ...state.listStaff]

    },
    getStaffReduce: (state, action:PayloadAction<Option>) => {
            state.isLoading = true
    },
    getStaffSuccess: (state, action:PayloadAction<ResponsiveData<Staff>>)=> {
        state.listStaff = action.payload.list
         state.total = action.payload.total;
        state.isLoading = false;

    },
    getStaffFail: (state)=> {
        state.isLoading = false;
    },
    updateStaff: (state, action:PayloadAction<Staff>) => {
        state.isLoading = true
    },
    updateStaffSuccess: (state, action:PayloadAction<Staff>)=> {
            const index = state.listStaff.findIndex(staff => staff.id === action.payload.id);

            if(index !== -1) {
                state.listStaff.splice(index, 1, action.payload);
            }
            state.isLoading = false
    },
    updateStaffFail: (state) => {
        state.isLoading = false
    },
    deleteStaff: (state, action:PayloadAction<number>)=> {
        state.isLoading = true;
    },
    deleteStaffSuccess: (state, action:PayloadAction<number>) => {
        state.listStaff = state.listStaff.filter(item => item.id !== action.payload);
        state.isLoading = false;
        
    },
    deleteStaffFail: (state)=> {
        state.isLoading = false;
    },
    searchStaff: (state, action:PayloadAction<{keyword:string, type: string ,option:Option}>) => {
        state.isLoading = true
    },
    searchStaffSuccess: (state, action:PayloadAction<ResponsiveData<Staff>>) => {
        state.listStaff = action.payload.list
        state.total = action.payload.total
        state.isLoading = false
    },
    searchStaffFail: (state) => {
        state.isLoading = false
    }

  },
})

export const { createStaffSuccess, getStaffReduce, 
    getStaffSuccess, getStaffFail, updateStaff, updateStaffSuccess, updateStaffFail,
     deleteStaff, deleteStaffFail,
      deleteStaffSuccess, searchStaff, searchStaffFail, searchStaffSuccess,
      setCurrentPage
    } = staffSlice.actions

export default staffSlice.reducer