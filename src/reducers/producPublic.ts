import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ProducPublicType, ProductItems } from "../typeProps/Productype";
import { Option } from "../types";

interface productReferent {
  total: number;
  loadding: boolean;
  error: string | null;
  productList: ProductItems[];
}

const initialState: productReferent = {
  total: 0,
  loadding: false,
  error: null,
  productList: [],
};

export const ProducPublic = createSlice({
  name: "productPublic",
  initialState,
  reducers: {
    getProducPublic: (state, action: PayloadAction<Option>) => {
      state.loadding = true;
    },
    getProductPublicSuccess: (
      state,
      action: PayloadAction<ProducPublicType>
    ) => {
      state.loadding = false;
    },
    getProducFail: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    },
  },
});

export const { getProducPublic, getProductPublicSuccess } =
  ProducPublic.actions;
