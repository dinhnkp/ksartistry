import { ConsoleSqlOutlined } from "@ant-design/icons";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { amountResult, amountType, searchAmountParam } from "../typeProps/Amounttype";
import { Option, Amount } from "../types";

interface amountInterface {
    loadding: boolean;
    error:null | string;
    total: number;
    currentPage: number;
    amountList: amountType[]
}

const initialState: amountInterface= {
    loadding:false,
    error:null,
    total: 0,
    currentPage: 1,
    amountList: []
}

export const  amountManagerSlice = createSlice({
    name: "amountManagerSlice",
    initialState,
    reducers: {
        setCurrenPage(state,action: PayloadAction<number>){
            state.currentPage = action.payload;
        },
        getAmounts(state) {
            state.loadding = true;
          },
        getAmount(state,action: PayloadAction<Option>){
            state.loadding= true;
        },
        getAmountSuccess (state,action:PayloadAction<amountResult>){
            state.total = action.payload.total;
            state.amountList = action.payload.list;
            state.loadding = false;
            
        },
        getAmountFail(state,action:PayloadAction<string>){
            state.error = action.payload;
        },
        searchAmount(state,action:PayloadAction<searchAmountParam>){
            state.loadding = true;
        },
        searchAmountSuccess(state,action:PayloadAction<amountResult>){
            state.total = action.payload.total;
            state.amountList = action.payload.list;
            state.loadding = false;
        },
        editAmount (state,action:PayloadAction<Amount>){
           
        },
        editAmountSuccess(state,action:PayloadAction<amountType>){
            const newLocation = state.amountList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item;
            })
            state.amountList = newLocation
        },
        putPriorityAmount (state,action:PayloadAction<number>){
        },
        putPrioritySuccess(state,action:PayloadAction<amountType>){
            const newState = state.amountList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item
            })
            state.amountList = newState;
        }
    }
});


export const {setCurrenPage, getAmounts, getAmount,getAmountSuccess,getAmountFail,searchAmount,searchAmountSuccess,editAmount,editAmountSuccess,putPriorityAmount,putPrioritySuccess} = amountManagerSlice.actions;

export default amountManagerSlice.reducer;