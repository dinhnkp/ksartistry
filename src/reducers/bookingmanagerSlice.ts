import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Booking, Option, ResponsiveData } from '../types'
// import {ModalState} from "../types"

// Define a type for the slice state
interface AuthState {
   bookingList: Booking[]
   isLoading: boolean,
   total: number,
   currentPage: number
}

// Define the initial state using that type
const initialState: AuthState = {
    bookingList: [],
    isLoading: false,
   total: 0,
   currentPage: 1

}

export const bookingSlice = createSlice({
  name: 'booking',
  initialState,
  reducers: {
    setCurrentPage: (state, action:PayloadAction<number>) => {
        state.currentPage = action.payload
    },
        getBooking: (state, action:PayloadAction<Option>) => {
            state.isLoading = true;
        },
        getBookingSuccess: (state, action:PayloadAction<ResponsiveData<Booking>>) => {
            state.bookingList = action.payload.list
            state.total = action.payload.total
            state.isLoading = false;
        },
        getBookingFail: (state) => {
            state.isLoading = false;
        },
        bookingStatusSuccess: (state, action:PayloadAction<Booking>)=> {
            const index = state.bookingList.findIndex(booking => booking.id === action.payload.id);
            state.bookingList.splice(index, 1, action.payload);
        },
        deleteBookingReduce: (state, action:PayloadAction<number>) => {
            state.isLoading = true;
        },
        deleteBookingSuccess: (state, action:PayloadAction<number>)=> {
            state.bookingList = state.bookingList.filter(booking => booking.id !== action.payload);
            state.isLoading = false
        },
        deleteBookingFail: (state) => {
            state.isLoading = false;
        },
        searchBooking: (state, action:PayloadAction<{keyword:string, option:Option}>) => {
            state.isLoading = true;
        },
        searchBookingSuccess: (state, action:PayloadAction<ResponsiveData<Booking>>) => {
            state.bookingList = action.payload.list
            state.total = action.payload.total
            state.isLoading = false;
        },
        searchBookingFail: (state) => {
            state.isLoading = false
        }
  
  },
})

export const {getBooking, getBookingSuccess, getBookingFail,
    bookingStatusSuccess, deleteBookingReduce, deleteBookingSuccess,
    deleteBookingFail, setCurrentPage,
    searchBooking, searchBookingSuccess, searchBookingFail
    } = bookingSlice.actions

export default bookingSlice.reducer