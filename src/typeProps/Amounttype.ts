import { Option } from "../types";

export type searchAmountParam = {
    type: string;
    keySearch: string;
    option: Option;
  }
  
  export type amountType = {
    id: number;
    name: string,
    price: string,
    description: string
  }
  
  export type amountTypePost = {
    name: string,
    price: string,
    description: string
  }
  
  export type amountResult = {total:number, list: amountType[]}