import { Option } from "../types";

export type searchStaffParam = {
    type: string;
    keySearch: string;
    option: Option;
  }
  
  export type staffType = {
    id: number;
    name: string,
    image: string,
    birthday: string,
    email: string,
    phoneNo: string,
    address: string,
    description: string
  }
  
  export type staffTypePost = {
    name: string,
    image: string,
    birthday: string,
    email: string,
    phoneNo: string,
    address: string,
    description: string
  }
  
  export type staffResult = {total:number, list: staffType[]}