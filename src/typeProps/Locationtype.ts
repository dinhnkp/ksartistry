import { Option } from "../types";

export type searchLocationParam = {
  type: string;
  keySearch: string;
  option: Option;
}

export type locationType = {
  id: number;
  name: string,
  address: string,
  description: string
}

export type locationTypePost = {
  name: string,
  address: string,
  description: string
}

export type locationResult = {total:number, list: locationType[]}